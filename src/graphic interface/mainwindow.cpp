#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWidgets>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    acciones << "check watch"<<"cross arms"<<"scratch head"<<"sit down"<<"get up"<<"turn around"<<"walk"<<"wave"<<"punch"<<"kick"<<"point"<<"pick up"<<"throw (over head)"<<"throw (from bottom up)";
    ui->setupUi(this);
    //QMainWindow::showMaximized();
    textEdit = new QPlainTextEdit;
    textEdit->setParent(ui->editor);

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();

    readSettings();

    connect(textEdit->document(), SIGNAL(contentsChanged()),
            this, SLOT(documentWasModified()));

    setCurrentFile("");
    setUnifiedTitleAndToolBarOnMac(true);

    connect(ui->ListVideosResultados, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(cargarValores(QListWidgetItem*)));
    connect(ui->ButtonCargarResultados, SIGNAL(clicked()), this, SLOT(cargarResultados()));
    connect(ui->TableEtiquetado, SIGNAL(itemPressed(QTableWidgetItem  *)), this, SLOT(cargarResultados(QTableWidgetItem  *)));
    connect(ui->buttonclean, SIGNAL(clicked()), this, SLOT(limpiarCajas()));
    connect(ui->buttonInitFrame, SIGNAL(clicked()), this, SLOT(setInitFrame()));
    connect(ui->buttonFinalFrame, SIGNAL(clicked()), this, SLOT(setFinalFrame()));
    connect(ui->buttonput, SIGNAL(clicked()), this, SLOT(put_text_line()));
    connect(ui->buttonChangePath, SIGNAL(clicked()), this, SLOT(changePathClass()));
    connect(ui->buttonExecuteClass, SIGNAL(clicked()), this, SLOT(executeClass()));
    connect(ui->buttonRecordChangePath, SIGNAL(clicked()), this, SLOT(changePathRecordKinect()));
    connect(ui->buttonRecordExeciuteKinect, SIGNAL(clicked()), this, SLOT(executeRecordKinect()));
    connect(ui->buttonRecordEexecuteConvert, SIGNAL(clicked()), this, SLOT(executeRecordConvert()));
    connect(ui->buttonRecordChangeBoWPath, SIGNAL(clicked()), this, SLOT(changeBoWDirectory()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

//! [2]

void MainWindow::changeBoWDirectory(){
    QFileDialog dialogDir(this,"Select BoW/ directory.");
    directorioBoW = dialogDir.getExistingDirectory();
    ui->lineEditRecordBoWDir->setText(directorioBoW);
}

void MainWindow::executeRecordConvert(){
    if( ui->lineEditRecordBoWDir->text() != ""){
        ui->plainTextEditRecordOutput->clear();
        QFileDialog dialogDir(this,"Selecione el directorio del script frame_to_video.sh");
        scriptConvertDirectory = dialogDir.getExistingDirectory();
        QString command("cd "+scriptConvertDirectory+" && sh frame_to_video.sh > result.txt");
        system(command.toStdString().c_str());

        QString fileName(scriptConvertDirectory+"/result.txt");
        QFile file(fileName);
        if (!file.open(QFile::ReadOnly | QFile::Text)) {
            QMessageBox::warning(this, tr("Application"),
                                 tr("Cannot read file %1:\n%2.")
                                 .arg(fileName)
                                 .arg(file.errorString()));
            return;
        }


        QTextStream in(&file);
        #ifndef QT_NO_CURSOR
        QApplication::setOverrideCursor(Qt::WaitCursor);
        #endif
        ui->plainTextEditRecordOutput->setPlainText(in.readAll());
        #ifndef QT_NO_CURSOR
        QApplication::restoreOverrideCursor();
        #endif

        command = "rm "+scriptConvertDirectory+"/result.txt";
        system(command.toStdString().c_str());

        command = "cp "+scriptConvertDirectory+"/videos/depth/*.avi "+directorioBoW+"/videos/";
        system(command.toStdString().c_str());

        command = "cp "+scriptConvertDirectory+"/videos/esqueleto/*.avi "+directorioBoW+"/videos/";
        system(command.toStdString().c_str());

        command = "cp "+scriptConvertDirectory+"/videos/rgb/*.avi "+directorioBoW+"/videos/";
        system(command.toStdString().c_str());
    }else{
        QMessageBox msgBox;
        msgBox.setText("Select BoW directory.");
        msgBox.exec();
    }
}

void MainWindow::executeRecordKinect(){
    if(ui->lineEditRecordDir->text() == ""){
        QMessageBox msgBox;
        msgBox.setText("Select output directory.");
        msgBox.exec();
    }else if(ui->lineEditRecordActorName->text() == ""){
        QMessageBox msgBox;
        msgBox.setText("Select actor name.");
        msgBox.exec();
    }else{
        ui->plainTextEditRecordOutput->clear();
        QFileDialog dialogDir(this,"Selecione el ejecutable Recorder");
        recorder_path = dialogDir.getExistingDirectory();
        QString command("cd "+recorder_path+" && ./Recorder "+ui->lineEditRecordDir->text() +"/actores/ "+ui->lineEditRecordActorName->text()+" > result.txt");
        system(command.toStdString().c_str());

        QString fileName(recorder_path+"/result.txt");
        QFile file(fileName);
        if (!file.open(QFile::ReadOnly | QFile::Text)) {
            QMessageBox::warning(this, tr("Application"),
                                 tr("Cannot read file %1:\n%2.")
                                 .arg(fileName)
                                 .arg(file.errorString()));
            return;
        }

        QTextStream in(&file);
        #ifndef QT_NO_CURSOR
        QApplication::setOverrideCursor(Qt::WaitCursor);
        #endif
        ui->plainTextEditRecordOutput->setPlainText(in.readAll());
        #ifndef QT_NO_CURSOR
        QApplication::restoreOverrideCursor();
        #endif

        command = "rm "+recorder_path+"/result.txt";
        system(command.toStdString().c_str());
    }
}

void MainWindow::changePathRecordKinect(){
    QFileDialog dialogDir(this,"Select output directory.");
    directory_output_RecordKinect = dialogDir.getExistingDirectory();
    ui->lineEditRecordDir->setText(directory_output_RecordKinect);
}


void MainWindow::executeClass(){
    if(directorioBoW == ""){
        QMessageBox msgBox;
        msgBox.setText("Select BoW path");
        msgBox.exec();
    }else{
        QString command("cd "+directorioBoW +"&& sh "+directorioBoW+"/run_all.sh > result.txt");
        system(command.toStdString().c_str());
        QString fileName(directorioBoW+"/result.txt");
        QFile file(fileName);
        if (!file.open(QFile::ReadOnly | QFile::Text)) {
            QMessageBox::warning(this, tr("Application"),
                                 tr("Cannot read file %1:\n%2.")
                                 .arg(fileName)
                                 .arg(file.errorString()));
            return;
        }

        QTextStream in(&file);
        #ifndef QT_NO_CURSOR
        QApplication::setOverrideCursor(Qt::WaitCursor);
        #endif
        ui->plainTextEditClassOutPut->setPlainText(in.readAll());
        #ifndef QT_NO_CURSOR
        QApplication::restoreOverrideCursor();
        #endif
    }
}

void MainWindow::changePathClass(){
    QFileDialog dialogDir(this,"Select BoW/ directory.");
    directorioBoW = dialogDir.getExistingDirectory();
    ui->lineEditClasBoWDir->setText(directorioBoW);
}

void MainWindow::put_text_line(){

    if(ui->listWidgetActions->currentRow()==-1){
        QMessageBox msgBox;
        msgBox.setText("Select an action");
        msgBox.exec();
    }else if(ui->lineEditInitFrame->text() == ""){
        QMessageBox msgBox;
        msgBox.setText("Select initial frame.");
        msgBox.exec();
    }else if(ui->lineEditFinalFrame->text() == ""){
        QMessageBox msgBox;
        msgBox.setText("Select final frame.");
        msgBox.exec();
    }else if(ui->lineEditInitFrame->text().toInt() > ui->lineEditFinalFrame->text().toInt()){
        QMessageBox msgBox;
        msgBox.setText("Final frame must be bigger than init frame");
        msgBox.exec();
    }else{
        textEdit->setPlainText(textEdit->toPlainText()+ui->lineEditInitFrame->text()+" "+ui->lineEditFinalFrame->text()+" "+ QString::number(ui->listWidgetActions->currentRow())+"\n");
    }
}

void MainWindow::set_player(Player *player){
    this->player=player;
}

void MainWindow::setInitFrame(){
    ui->lineEditInitFrame->setText(QString::number(player->player->position()/40));
}

void MainWindow::setFinalFrame(){
    ui->lineEditFinalFrame->setText(QString::number(player->player->position()/40));
}

void MainWindow::limpiarCajas(){
    ui->lineEditInitFrame->clear();
    ui->lineEditFinalFrame->clear();
}

void MainWindow::cargarValores(QListWidgetItem* itm)
//! [3] //! [4]
{
    //Variables:
    QString line;
    QStringList strList;
    int action;
    int fila=0;

    QString fileName(directorioBoW+"/videos/truth/"+itm->text()+"_frame_segmented.txt");
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    QString fileName2(directorioBoW+"/recognition_process/results/classified_result_0_400_hof_300_"+itm->text());
    QFile file2(fileName2);
    if (!file2.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName2)
                             .arg(file2.errorString()));
        return;
    }

    QTextStream in(&file);
    #ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
    #endif

    QTextStream in2(&file2);
    #ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
    #endif

    while(!in.atEnd()){
        line = in.readLine();
        strList = line.split(" ");
        ui->TableEtiquetado->setItem(fila,0, new QTableWidgetItem(strList[0]));
        ui->TableEtiquetado->setItem(fila,1, new QTableWidgetItem(strList[1]));
        ui->TableEtiquetado->setItem(fila,2, new QTableWidgetItem(acciones[strList[2].toInt()-1]));
        action = strList[2].toInt();

        line = in2.readLine();
        strList = line.split(" ");
        ui->TableResultado->setItem(fila,0, new QTableWidgetItem(acciones[strList[0].toInt()-1]));

        if(strList[0].toInt() == action){
            ui->TableResultado->setItem(fila,1, new QTableWidgetItem("Yes"));
            ui->TableResultado->item(fila,0)->setBackgroundColor(Qt::green);
            ui->TableResultado->item(fila,1)->setBackgroundColor(Qt::green);
        }else{
            ui->TableResultado->setItem(fila,1, new QTableWidgetItem("No"));
            ui->TableResultado->item(fila,0)->setBackgroundColor(Qt::red);
            ui->TableResultado->item(fila,1)->setBackgroundColor(Qt::red);
        }
        fila++;
    }

    QString fileName3(directorioBoW+"/recognition_process/results/accuracy_0_400_hof_300_"+itm->text());
    QFile file3(fileName3);
    if (!file3.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName3)
                             .arg(file3.errorString()));
        return;
    }

    QTextStream in3(&file3);
    #ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
    #endif
    ui->LavelAccuracy->setText(in3.readAll());
    #ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
    #endif

    statusBar()->showMessage(tr("Resultado cargado correctamente"), 2000);
}

void MainWindow::cargarResultados(QTableWidgetItem  *item){
    ui->TableResultado->selectRow(item->row());
}

void MainWindow::cargarResultados()
//! [3] //! [4]
{
    QStringList strList;
    DIR* directorio;
    struct dirent* elemento;
    QFileDialog dialogDir(this,"Selecione el directorio BoW/");

    directorioBoW = dialogDir.getExistingDirectory();
    ui->ListVideosResultados->clear();
    directorio=opendir ((directorioBoW + "/recognition_process/results/").toStdString().c_str());

    if (directorio!=NULL) {
        elemento=readdir (directorio);

        while (elemento!=NULL) {
            QString aux (elemento->d_name);

            if(aux.contains("accuracy")){
                strList = aux.split("_");
                QListWidgetItem *itm = new QListWidgetItem;
                itm->setText(strList[5]);
                ui->ListVideosResultados->addItem(itm);
            }

            elemento=readdir (directorio);
        }

        closedir (directorio);
    }else{
        QMessageBox msgBox;
        msgBox.setText("Directory invalid.");
        msgBox.exec();
        printf ("Error de Lectura en el Directorio \n");
    }
}

//! [3]
void MainWindow::closeEvent(QCloseEvent *event)
//! [3] //! [4]
{
    if (maybeSave()) {
        writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
}
//! [4]

//! [5]
void MainWindow::newFile()
//! [5] //! [6]
{
    if (maybeSave()) {
        textEdit->clear();
        setCurrentFile("");
    }
}
//! [6]

//! [7]
void MainWindow::open()
//! [7] //! [8]
{
    if (maybeSave()) {
        QString fileName = QFileDialog::getOpenFileName(this);
        if (!fileName.isEmpty())
            loadFile(fileName);
    }
}
//! [8]

//! [9]
bool MainWindow::save()
//! [9] //! [10]
{
    if (curFile.isEmpty()) {
        return saveAs();
    } else {
        return saveFile(curFile);
    }
}
//! [10]

//! [11]
bool MainWindow::saveAs()
//! [11] //! [12]
{
    QFileDialog dialogFile(this);
    dialogFile.setDefaultSuffix("_frame_segmented.txt");
    QString fileName = dialogFile.getSaveFileName(this,QString::fromUtf8("Save file - XML distr."), tr("ACTORNAME_frame_segmented.txt"), tr("_frame_segmented.txt"));
    if (fileName.isEmpty())
        return false;

    return saveFile(fileName);
}
//! [12]

//! [13]
void MainWindow::about()
//! [13] //! [14]
{
    QMessageBox::about(this, tr("About Application"),
        tr("The <b>Application</b> example demonstrates how to "
           "write modern GUI applications using Qt, with a menu bar, "
           "toolbars, and a status bar."));
}
//! [14]

//! [15]
void MainWindow::documentWasModified()
//! [15] //! [16]
{
    setWindowModified(textEdit->document()->isModified());
}
//! [16]

//! [17]
void MainWindow::createActions()
//! [17] //! [18]
{
    newAct = new QAction(QIcon(":/images/new.png"), tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new file"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

    //! [19]
    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));
    //! [18] //! [19]

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(tr("Save &As..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save the document under a new name"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    //! [20]
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    //! [20]
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    //! [21]
    cutAct = new QAction(QIcon(":/images/cut.png"), tr("Cu&t"), this);
    //! [21]
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                            "clipboard"));
    connect(cutAct, SIGNAL(triggered()), textEdit, SLOT(cut()));

    copyAct = new QAction(QIcon(":/images/copy.png"), tr("&Copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                             "clipboard"));
    connect(copyAct, SIGNAL(triggered()), textEdit, SLOT(copy()));

    pasteAct = new QAction(QIcon(":/images/paste.png"), tr("&Paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                              "selection"));
    connect(pasteAct, SIGNAL(triggered()), textEdit, SLOT(paste()));

    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    //! [22]
    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    //! [22]

    //! [23]
    cutAct->setEnabled(false);
    //! [23] //! [24]
    copyAct->setEnabled(false);
    connect(textEdit, SIGNAL(copyAvailable(bool)),
            cutAct, SLOT(setEnabled(bool)));
    connect(textEdit, SIGNAL(copyAvailable(bool)),
            copyAct, SLOT(setEnabled(bool)));
}
//! [24]

//! [25] //! [26]
void MainWindow::createMenus()
//! [25] //! [27]
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    //! [28]
    fileMenu->addAction(openAct);
    //! [28]
    fileMenu->addAction(saveAct);
    //! [26]
    fileMenu->addAction(saveAsAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);

    menuBar()->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
}
//! [27]

//! [29] //! [30]
void MainWindow::createToolBars()
{
    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(newAct);
    //! [29] //! [31]
    fileToolBar->addAction(openAct);
    //! [31]
    fileToolBar->addAction(saveAct);

    editToolBar = addToolBar(tr("Edit"));
    editToolBar->addAction(cutAct);
    editToolBar->addAction(copyAct);
    editToolBar->addAction(pasteAct);
}
//! [30]

//! [32]
void MainWindow::createStatusBar()
//! [32] //! [33]
{
    statusBar()->showMessage(tr("Ready"));
}
//! [33]

//! [34] //! [35]
void MainWindow::readSettings()
//! [34] //! [36]
{
    QSettings settings("QtProject", "Application Example");
    QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings.value("size", QSize(400, 400)).toSize();
    resize(size);
    move(pos);
}
//! [35] //! [36]

//! [37] //! [38]
void MainWindow::writeSettings()
//! [37] //! [39]
{
    QSettings settings("QtProject", "Application Example");
    settings.setValue("pos", pos());
    settings.setValue("size", size());
}
//! [38] //! [39]

//! [40]
bool MainWindow::maybeSave()
//! [40] //! [41]
{
    if (textEdit->document()->isModified()) {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Application"),
                     tr("The document has been modified.\n"
                        "Do you want to save your changes?"),
                     QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;
    }
    return true;
}
//! [41]

//! [42]
void MainWindow::loadFile(const QString &fileName)
//! [42] //! [43]
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    QTextStream in(&file);
    #ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
    #endif
    textEdit->setPlainText(in.readAll());
    #ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
    #endif

    setCurrentFile(fileName);
    statusBar()->showMessage(tr("File loaded"), 2000);
}
//! [43]

//! [44]
bool MainWindow::saveFile(const QString &fileName)
//! [44] //! [45]
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return false;
    }

    QTextStream out(&file);
    #ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
    #endif
    out << textEdit->toPlainText();
    #ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
    #endif

    setCurrentFile(fileName);
    statusBar()->showMessage(tr("File saved"), 2000);
    return true;
}
//! [45]

//! [46]
void MainWindow::setCurrentFile(const QString &fileName)
//! [46] //! [47]
{
    curFile = fileName;
    textEdit->document()->setModified(false);
    setWindowModified(false);

    QString shownName = curFile;
    if (curFile.isEmpty())
        shownName = "untitled.txt";
    setWindowFilePath(shownName);
}
//! [47]

//! [48]
QString MainWindow::strippedName(const QString &fullFileName)
//! [48] //! [49]
{
    return QFileInfo(fullFileName).fileName();
}
//! [49]

class MyListItem : QListWidgetItem
{
public:
    QString *filename;
};
