#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QListWidgetItem"
#include "QTableWidgetItem"
#include "player.h"

QT_BEGIN_NAMESPACE
    class QAction;
    class QMenu;
    class QPlainTextEdit;
QT_END_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

protected:
    void closeEvent(QCloseEvent *event);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Ui::MainWindow *ui;
    Player *player;
    void set_player(Player *player);

private slots:
    void newFile();
    void open();
    bool save();
    bool saveAs();
    void about();
    void documentWasModified();
    void cargarResultados();
    void cargarValores( QListWidgetItem* itm);
    void cargarResultados(QTableWidgetItem  *item);
    void limpiarCajas();
    void setInitFrame();
    void setFinalFrame();
    void put_text_line();
    void changePathClass();
    void executeClass();
    void changePathRecordKinect();
    void executeRecordKinect();
    void executeRecordConvert();
    void changeBoWDirectory();

private:

    void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();
    void readSettings();
    void writeSettings();
    bool maybeSave();
    void loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);
    QString strippedName(const QString &fullFileName);

    QPlainTextEdit *textEdit;
    QString curFile;

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *helpMenu;
    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *exitAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *aboutAct;
    QAction *aboutQtAct;
    QStringList acciones;
    QString directorioBoW;
    QString recorder_path;
    QString directory_output_RecordKinect;
    QString scriptConvertDirectory;
};

#endif // MAINWINDOW_H
