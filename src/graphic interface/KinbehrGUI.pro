TEMPLATE = app
TARGET = KinbehrGUI

QT += network \
      xml \
      multimedia \
      multimediawidgets \
      widgets

HEADERS = \
    player.h \
    playercontrols.h \
    playlistmodel.h \
    videowidget.h \
    histogramwidget.h \
    mainwindow.h
SOURCES = main.cpp \
    player.cpp \
    playercontrols.cpp \
    playlistmodel.cpp \
    videowidget.cpp \
    histogramwidget.cpp \
    mainwindow.cpp

maemo* {
    DEFINES += PLAYER_NO_COLOROPTIONS
}

target.path = /home/rubencn/kinbehr/KinbehrGUI/
INSTALLS += target

FORMS += \
    mainwindow.ui

RESOURCES += \
    Resources.qrc
