#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "player.h"

#include <QApplication>

int main(int argc, char *argv[])
{
#ifdef Q_WS_MAEMO_6
    QApplication::setGraphicsSystem("raster");
#endif
    QApplication app(argc, argv);
    MainWindow w;

    Player player;
    player.setParent( w.ui->video);

    w.set_player(&player);

    #if defined(Q_WS_SIMULATOR)
        player.setAttribute(Qt::WA_LockLandscapeOrientation);
        player.showMaximized();
    #else
        player.show();
    #endif

    w.show();
    return app.exec();
}
