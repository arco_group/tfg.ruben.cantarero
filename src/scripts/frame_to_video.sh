#!/bin/bash

DIR=tmp
DEP=depth
RGB=rgb
VID=videos
ACT=actores
ESQ=esqueleto

echo Creando directorios para los videos...
mkdir $VID
mkdir $VID/$DEP
mkdir $VID/$RGB
mkdir $VID/$ESQ
echo ...directorios creados correctamente.

for dire in $(ls  -D $ACT); do
	echo Preparando todo para $dire

	#DEPTH
	mkdir $ACT/$dire/$DEP/tmp
	cd $ACT/$dire/$DEP/tmp
	index=0
	for file in $(ls ../*.jpg |sort -t "_" -n -k2,2); do
	    name=$(echo $file | cut -d"_" -f1)
	    ln -s $file $(basename ${name}_${index}.jpg)
	    index=$((index+1))
	done
	cd ../../../../
	echo Generando video DEPHT para $dire
	ffmpeg -r 25 -i $ACT/$dire/$DEP/$DIR/$(basename ${name}_%d.jpg) -vb 10M ./$VID/$DEP/$(basename ${name}Depth.avi)
	rm -fr $ACT/$dire/$DEP/$DIR/
	
	
	#RGB
	mkdir $ACT/$dire/$RGB/tmp
	cd $ACT/$dire/$RGB/tmp
	index=0
	for file in $(ls ../*.jpg |sort -t "_" -n -k2,2); do
	    name=$(echo $file | cut -d"_" -f1)
	    ln -s $file $(basename ${name}_${index}.jpg)
	    index=$((index+1))
	done
	cd ../../../../
	echo Generando video RGB para $dire
	ffmpeg -r 25 -i $ACT/$dire/$RGB/$DIR/$(basename ${name}_%d.jpg) -vb 10M ./$VID/$RGB/$(basename ${name}Rgb.avi)
	rm -fr $ACT/$dire/$RGB/$DIR/

	#ESQUELETO
	mkdir $ACT/$dire/$ESQ/tmp
	cd $ACT/$dire/$ESQ/tmp
	index=0
	for file in $(ls ../*.jpg |sort -t "_" -n -k2,2); do
	    name=$(echo $file | cut -d"_" -f1)
	    ln -s $file $(basename ${name}_${index}.jpg)
	    index=$((index+1))
	done
	cd ../../../../
	echo Generando video RGB para $dire
	ffmpeg -r 25 -i $ACT/$dire/$ESQ/$DIR/$(basename ${name}_%d.jpg) -vb 10M ./$VID/$ESQ/$(basename ${name}Esqueleto.avi)
	rm -fr $ACT/$dire/$ESQ/$DIR/


	echo Finalizado todo para $dire
done








