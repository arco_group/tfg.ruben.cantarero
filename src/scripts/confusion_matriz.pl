#! /usr/bin/perl -w
use strict;
use warnings;

my ($dir_results,$dir_truth, @FILES_results, @FILES_truth,$file, @truth,$filename,@splt,@result, @frame_line, $truth_action,$x, $y, $value);

my @action1 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action2= (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action3 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action4 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action5 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action6 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action7 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action8 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action9 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action10 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action11 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action12 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action13 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
my @action14 = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

my @confusion_matrix = (\@action1,\@action2,\@action3,\@action4,\@action5,\@action6,\@action7,\@action8,\@action9,\@action10,\@action11,\@action12,\@action13,\@action14);

$dir_results = "./recognition_process/results/";
$dir_truth = "./videos/truth/";

opendir DIR1, "$dir_results" || die "Problema al leer directorio";

open (OUTPUT,">matrix_confusion.csv");

@FILES_results = readdir(DIR1);

foreach $file (@FILES_results) {
	if($file =~ /classified_result_0_400_hof_300_/ ) {
		$filename = $dir_results . $file ; 
		
		#print $filename, "\n";
  		open(INPUT,"< $filename") or die("open: $!");
		@result=<INPUT>;

		#print @truth, "\n";

		@splt=split(/_/,$file);
		@splt=split(/\./,$splt[-1]);

		$filename = $dir_truth . $splt[0] . "_frame_segmented.txt" ;
		#print $filename, "\n";

		open(INPUT,"<$filename") or die("open: $!");
		@truth=<INPUT>;
	
		#print @result, "\n";

		foreach my $i (0..$#truth){
			chomp($truth[$i]);
			@frame_line = split(/ /, $truth[$i]);
			$truth_action = $frame_line[2];

			$confusion_matrix[$truth_action-1][$result[$i]]++;
			$confusion_matrix[$truth_action-1][0]++;#Total
		}
	}	
}

foreach $x (0..@confusion_matrix-1){
	foreach $y (0..@{$confusion_matrix[$x]}-1){
		if($y != 0){
			$value = sprintf("%.2f",$confusion_matrix[$x][$y]/$confusion_matrix[$x][0]);
		}else{
			$value = $confusion_matrix[$x][$y]
		}	

		print "\t$value";
		print(OUTPUT $value);

		if($y != (@{$confusion_matrix[$x]}-1)){
			print(OUTPUT ",");
		}
	}
	print "\n";
	print(OUTPUT "\n");
}


closedir(DIR1);
close(INPUT);
close(OUTPUT);
