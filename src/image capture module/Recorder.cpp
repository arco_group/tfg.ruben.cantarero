#if (defined _WIN32)
#define PRIu64 "llu"
#else
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#endif

#include "Recorder.h"
#include <NiteSampleUtilities.h>


#if (ONI_PLATFORM == ONI_PLATFORM_MACOSX)
        #include <GLUT/glut.h>
#else
        #include <GL/glut.h>
#endif

#define GL_WIN_SIZE_X	1280
#define GL_WIN_SIZE_Y	1024
#define TEXTURE_SIZE	512
#define DEFAULT_DISPLAY_MODE	DISPLAY_MODE_DEPTH
#define MIN_NUM_CHUNKS(data_size, chunk_size)	((((data_size)-1) / (chunk_size) + 1))
#define MIN_CHUNKS_SIZE(data_size, chunk_size)	(MIN_NUM_CHUNKS(data_size, chunk_size) * (chunk_size))
#define MAX_USERS 10

Recorder* Recorder::ms_self = NULL;

using namespace std;
using namespace cv;
using namespace tinyxml2;

bool g_visibleUsers[MAX_USERS] = {false};
nite::SkeletonState g_skeletonStates[MAX_USERS] = {nite::SKELETON_NONE};
char g_userStatusLabels[MAX_USERS][100] = {{0}};
char g_generalMessage[100] = {0};
bool g_drawSkeleton = true;
bool g_drawCenterOfMass = false;
bool g_drawStatusLabel = true;
bool g_drawBoundingBox = false;
bool g_drawBackground = true;
bool g_drawDepth = true;
bool g_drawFrameId = false;
bool capturar=false;
bool capturar_rgb=false;
bool capturar_depth=false;
bool mostrar_video=true; 
char *filename;
char *filepath;
float Colors[][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {1, 1, 1}};
int colorCount = 3;
list <thread> hilos;
openni::Device device;
openni::VideoStream  color;
openni::VideoFrameRef colorFrame;
openni::VideoStream  depth;
openni::VideoFrameRef depthFrame;

int g_nXRes = 0, g_nYRes = 0;


void Recorder::glutIdle()
{
	glutPostRedisplay();
}
void Recorder::glutDisplay()
{
	Recorder::ms_self->Display();
}
void Recorder::glutKeyboard(unsigned char key, int x, int y)
{
	Recorder::ms_self->OnKey(key, x, y);
}

Recorder::Recorder(const char* strSampleName) : m_poseUser(0)
{
	ms_self = this;
	strncpy(m_strSampleName, strSampleName, ONI_MAX_STR);
	m_pUserTracker = new nite::UserTracker;
}
Recorder::~Recorder()
{
	Finalize();

	delete[] m_pTexMap;

	ms_self = NULL;
}

void Recorder::Finalize()
{
	delete m_pUserTracker;
	nite::NiTE::shutdown();
	openni::OpenNI::shutdown();
}

openni::Status Recorder::Init(int argc, char **argv)
{
	m_pTexMap = NULL;

	openni::Status rc = openni::OpenNI::initialize();
	if (rc != openni::STATUS_OK)
	{
		printf("Failed to initialize OpenNI\n%s\n", openni::OpenNI::getExtendedError());
		return rc;
	}

	const char* deviceUri = openni::ANY_DEVICE;
	for (int i = 1; i < argc-1; ++i)
	{
		if (strcmp(argv[i], "-device") == 0)
		{
			deviceUri = argv[i+1];
			break;
		}
	}

	if(argc==3){
		filepath=argv[1];
		filename=argv[2];
		char buffer[500];
		sprintf (buffer, "mkdir -p %s/%s && mkdir -p /%s/%s/rgb && mkdir -p /%s/%s/depth/xml && mkdir -p /%s/%s/esqueleto", filepath ,filename,filepath ,filename,filepath ,filename,filepath ,filename);
 		system (buffer);
	}else{
		printf("\nInvalid arguments. Must be: ./executable_name_path name\n");
		exit(1);
	}

	rc = m_device.open(deviceUri);
	if (rc != openni::STATUS_OK)
	{
		printf("Failed to open device\n%s\n", openni::OpenNI::getExtendedError());
		return rc;
	}

	nite::NiTE::initialize();

	if (m_pUserTracker->create(&m_device) != nite::STATUS_OK)
	{
		return openni::STATUS_ERROR;
	}


	rc = device.open(openni::ANY_DEVICE);
	rc = color.create(device, openni::SENSOR_COLOR);
	rc = color.start();
	if (rc != openni::STATUS_OK)
	{
		printf("Couldn't start stream SENSOR_COLOR\n");
		openni::OpenNI::shutdown();
	}
	
	return InitOpenGL(argc, argv);

}
openni::Status Recorder::Run()
{
	glutMainLoop();

	return openni::STATUS_OK;
}



#define USER_MESSAGE(msg) {\
	sprintf(g_userStatusLabels[user.getId()], "%s", msg);\
	printf("[%08" PRIu64 "] User #%d:\t%s\n", ts, user.getId(), msg);}

void updateUserState(const nite::UserData& user, uint64_t ts)
{
	if (user.isNew())
	{
		USER_MESSAGE("New");
	}
	else if (user.isVisible() && !g_visibleUsers[user.getId()])
		printf("[%08" PRIu64 "] User #%d:\tVisible\n", ts, user.getId());
	else if (!user.isVisible() && g_visibleUsers[user.getId()])
		printf("[%08" PRIu64 "] User #%d:\tOut of Scene\n", ts, user.getId());
	else if (user.isLost())
	{
		USER_MESSAGE("Lost");
	}
	g_visibleUsers[user.getId()] = user.isVisible();


	if(g_skeletonStates[user.getId()] != user.getSkeleton().getState())
	{
		switch(g_skeletonStates[user.getId()] = user.getSkeleton().getState())
		{
		case nite::SKELETON_NONE:
			USER_MESSAGE("Stopped tracking.")
			break;
		case nite::SKELETON_CALIBRATING:
			USER_MESSAGE("Calibrating...")
			break;
		case nite::SKELETON_TRACKED:
			USER_MESSAGE("Tracking!")
			break;
		case nite::SKELETON_CALIBRATION_ERROR_NOT_IN_POSE:
		case nite::SKELETON_CALIBRATION_ERROR_HANDS:
		case nite::SKELETON_CALIBRATION_ERROR_LEGS:
		case nite::SKELETON_CALIBRATION_ERROR_HEAD:
		case nite::SKELETON_CALIBRATION_ERROR_TORSO:
			USER_MESSAGE("Calibration Failed... :-|")
			break;
		}
	}
}

#ifndef USE_GLES
void glPrintString(void *font, const char *str)
{
	int i,l = (int)strlen(str);

	for(i=0; i<l; i++)
	{   
		glutBitmapCharacter(font,*str++);
	}   
}
#endif
void DrawStatusLabel(nite::UserTracker* pUserTracker, const nite::UserData& user)
{
	int color = user.getId() % colorCount;
	glColor3f(1.0f - Colors[color][0], 1.0f - Colors[color][1], 1.0f - Colors[color][2]);

	float x,y;
	pUserTracker->convertJointCoordinatesToDepth(user.getCenterOfMass().x, user.getCenterOfMass().y, user.getCenterOfMass().z, &x, &y);
	x *= GL_WIN_SIZE_X/(float)g_nXRes;
	y *= GL_WIN_SIZE_Y/(float)g_nYRes;
	char *msg = g_userStatusLabels[user.getId()];
	glRasterPos2i(x-((strlen(msg)/2)*8),y);
	glPrintString(GLUT_BITMAP_HELVETICA_18, msg);
}
void DrawFrameId(int frameId)
{
	char buffer[80] = "";
	sprintf(buffer, "%d", frameId);
	glColor3f(1.0f, 0.0f, 0.0f);
	glRasterPos2i(20, 20);
	glPrintString(GLUT_BITMAP_HELVETICA_18, buffer);
}
void DrawCenterOfMass(nite::UserTracker* pUserTracker, const nite::UserData& user)
{
	glColor3f(1.0f, 1.0f, 1.0f);
	char buffer[80] = "";
	sprintf(buffer,"X:%1.0f  Y:%1.0f Z:%1.0f",user.getCenterOfMass().x, user.getCenterOfMass().y, user.getCenterOfMass().z);

	float coordinates[3] = {0};

	pUserTracker->convertJointCoordinatesToDepth(user.getCenterOfMass().x, user.getCenterOfMass().y, user.getCenterOfMass().z, &coordinates[0], &coordinates[1]);

	coordinates[0] *= GL_WIN_SIZE_X/(float)g_nXRes;
	coordinates[1] *= GL_WIN_SIZE_Y/(float)g_nYRes;
	glPointSize(8);
	glVertexPointer(3, GL_FLOAT, 0, coordinates);
	glDrawArrays(GL_POINTS, 0, 1);

	glColor3f(1.0f, 1.0f, 1.0f);
	glRasterPos2i(coordinates[0], coordinates[1]+20);
	glPrintString(GLUT_BITMAP_HELVETICA_10, buffer);

}
void DrawBoundingBox(const nite::UserData& user)
{
	glColor3f(1.0f, 1.0f, 1.0f);

	float coordinates[] =
	{
		user.getBoundingBox().max.x, user.getBoundingBox().max.y, 0,
		user.getBoundingBox().max.x, user.getBoundingBox().min.y, 0,
		user.getBoundingBox().min.x, user.getBoundingBox().min.y, 0,
		user.getBoundingBox().min.x, user.getBoundingBox().max.y, 0,
	};
	coordinates[0]  *= GL_WIN_SIZE_X/(float)g_nXRes;
	coordinates[1]  *= GL_WIN_SIZE_Y/(float)g_nYRes;
	coordinates[3]  *= GL_WIN_SIZE_X/(float)g_nXRes;
	coordinates[4]  *= GL_WIN_SIZE_Y/(float)g_nYRes;
	coordinates[6]  *= GL_WIN_SIZE_X/(float)g_nXRes;
	coordinates[7]  *= GL_WIN_SIZE_Y/(float)g_nYRes;
	coordinates[9]  *= GL_WIN_SIZE_X/(float)g_nXRes;
	coordinates[10] *= GL_WIN_SIZE_Y/(float)g_nYRes;

	glPointSize(2);
	glVertexPointer(3, GL_FLOAT, 0, coordinates);
	glDrawArrays(GL_LINE_LOOP, 0, 4);

}



void DrawLimb(nite::UserTracker* pUserTracker, const nite::SkeletonJoint& joint1, const nite::SkeletonJoint& joint2, int color)
{
	float coordinates[6] = {0};
	pUserTracker->convertJointCoordinatesToDepth(joint1.getPosition().x, joint1.getPosition().y, joint1.getPosition().z, &coordinates[0], &coordinates[1]);
	pUserTracker->convertJointCoordinatesToDepth(joint2.getPosition().x, joint2.getPosition().y, joint2.getPosition().z, &coordinates[3], &coordinates[4]);

	coordinates[0] *= GL_WIN_SIZE_X/(float)g_nXRes;
	coordinates[1] *= GL_WIN_SIZE_Y/(float)g_nYRes;
	coordinates[3] *= GL_WIN_SIZE_X/(float)g_nXRes;
	coordinates[4] *= GL_WIN_SIZE_Y/(float)g_nYRes;

	if (joint1.getPositionConfidence() == 1 && joint2.getPositionConfidence() == 1)
	{
		glColor3f(1.0f - Colors[color][0], 1.0f - Colors[color][1], 1.0f - Colors[color][2]);
	}
	else if (joint1.getPositionConfidence() < 0.5f || joint2.getPositionConfidence() < 0.5f)
	{
		return;
	}
	else
	{
		glColor3f(1.0f, 1.0f - joint1.getPositionConfidence(), 1.0f -joint1.getPositionConfidence());
	}
	glPointSize(2);
	glVertexPointer(3, GL_FLOAT, 0, coordinates);
	glDrawArrays(GL_LINES, 0, 2);

	glPointSize(10);
	if (joint1.getPositionConfidence() == 1)
	{
		glColor3f(1.0f - Colors[color][0], 1.0f - Colors[color][1], 1.0f - Colors[color][2]);
	}
	else
	{
		glColor3f(1.0f , 1.0f - joint1.getPositionConfidence(), 1.0f - joint1.getPositionConfidence());
	}
	glVertexPointer(3, GL_FLOAT, 0, coordinates);
	glDrawArrays(GL_POINTS, 0, 1);

	if (joint2.getPositionConfidence() == 1)
	{
		glColor3f(1.0f - Colors[color][0], 1.0f - Colors[color][1], 1.0f - Colors[color][2]);
	}
	else
	{
		glColor3f(1.0f , 1.0f - joint1.getPositionConfidence(), 1.0f - joint1.getPositionConfidence());
	}
	glVertexPointer(3, GL_FLOAT, 0, coordinates+3);
	glDrawArrays(GL_POINTS, 0, 1);


	char buffer[80] = "";
	sprintf(buffer,"X:%1.0f  Y:%1.0f Z:%1.0f",joint1.getPosition().x, joint1.getPosition().y, joint1.getPosition().z);
	glColor3f(1.0f, 1.0f, 1.0f);
	glRasterPos2i(coordinates[0], coordinates[1]);
	glPrintString(GLUT_BITMAP_HELVETICA_10, buffer);

	sprintf(buffer,"X:%1.0f  Y:%1.0f Z:%1.0f",joint2.getPosition().x, joint2.getPosition().y, joint2.getPosition().z);
	glColor3f(1.0f, 1.0f, 1.0f);
	glRasterPos2i(coordinates[3], coordinates[4]);
	glPrintString(GLUT_BITMAP_HELVETICA_10, buffer);

}

void DrawSkeleton(nite::UserTracker* pUserTracker, const nite::UserData& userData)
{
	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_HEAD), userData.getSkeleton().getJoint(nite::JOINT_NECK), userData.getId() % colorCount);

	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_LEFT_SHOULDER), userData.getSkeleton().getJoint(nite::JOINT_LEFT_ELBOW), userData.getId() % colorCount);
	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_LEFT_ELBOW), userData.getSkeleton().getJoint(nite::JOINT_LEFT_HAND), userData.getId() % colorCount);

	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_RIGHT_SHOULDER), userData.getSkeleton().getJoint(nite::JOINT_RIGHT_ELBOW), userData.getId() % colorCount);
	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_RIGHT_ELBOW), userData.getSkeleton().getJoint(nite::JOINT_RIGHT_HAND), userData.getId() % colorCount);

	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_LEFT_SHOULDER), userData.getSkeleton().getJoint(nite::JOINT_RIGHT_SHOULDER), userData.getId() % colorCount);

	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_LEFT_SHOULDER), userData.getSkeleton().getJoint(nite::JOINT_TORSO), userData.getId() % colorCount);
	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_RIGHT_SHOULDER), userData.getSkeleton().getJoint(nite::JOINT_TORSO), userData.getId() % colorCount);

	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_TORSO), userData.getSkeleton().getJoint(nite::JOINT_LEFT_HIP), userData.getId() % colorCount);
	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_TORSO), userData.getSkeleton().getJoint(nite::JOINT_RIGHT_HIP), userData.getId() % colorCount);

	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_LEFT_HIP), userData.getSkeleton().getJoint(nite::JOINT_RIGHT_HIP), userData.getId() % colorCount);


	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_LEFT_HIP), userData.getSkeleton().getJoint(nite::JOINT_LEFT_KNEE), userData.getId() % colorCount);
	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_LEFT_KNEE), userData.getSkeleton().getJoint(nite::JOINT_LEFT_FOOT), userData.getId() % colorCount);

	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_RIGHT_HIP), userData.getSkeleton().getJoint(nite::JOINT_RIGHT_KNEE), userData.getId() % colorCount);
	DrawLimb(pUserTracker, userData.getSkeleton().getJoint(nite::JOINT_RIGHT_KNEE), userData.getSkeleton().getJoint(nite::JOINT_RIGHT_FOOT), userData.getId() % colorCount);
}



void save_rgb(char *filepath,char *filename,openni::VideoFrameRef colorFrame ,const openni::RGB888Pixel* pPixel){
	try{
		char buffer[500] = "";
		sprintf(buffer,"/%s/%s/rgb/%s_%d_%lu_rgb.jpg",filepath,filename,filename,colorFrame.getFrameIndex(),colorFrame.getTimestamp());
		
		Mat colorImage; 
		Mat colorArr[3]; 
		colorArr[0] = Mat(colorFrame.getVideoMode().getResolutionY(),colorFrame.getVideoMode().getResolutionX(),CV_8U); 
		colorArr[1] = Mat(colorFrame.getVideoMode().getResolutionY(),colorFrame.getVideoMode().getResolutionX(),CV_8U); 
		colorArr[2] = Mat(colorFrame.getVideoMode().getResolutionY(),colorFrame.getVideoMode().getResolutionX(),CV_8U); 		

		for (int y = 0; y < colorFrame.getHeight(); ++y){
			uchar* Bptr = colorArr[0].ptr<uchar>(y); 
        		uchar* Gptr = colorArr[1].ptr<uchar>(y); 
        		uchar* Rptr = colorArr[2].ptr<uchar>(y);
			for (int x = 0; x < colorFrame.getWidth(); ++x){
				Bptr[x] = pPixel->b; 
                		Gptr[x] = pPixel->g; 
                		Rptr[x] = pPixel->r; 
				++pPixel;
			}
		}

		merge(colorArr,3,colorImage); 
		IplImage bgrIpl = colorImage;    
		cvSaveImage(buffer,&bgrIpl);

	}catch (std::exception& e) {
		printf("Expection in save_rgb(): %s\n",e.what());
	}
}

void set_metadatos(char* xml_filename,const nite::Array<nite::UserData>& users, openni::VideoFrameRef frame,long int timestamp){
	char buffer[100] = "";	
	XMLDocument* doc = new XMLDocument();
	doc->InsertEndChild(doc->NewDeclaration("\xef\xbb\xbfxml version=\"1.0\" encoding=\"UTF-8\""));
	XMLNode* xmlframe_node = doc->InsertEndChild( doc->NewElement( "frame" ) );
	
	sprintf(buffer,"%ld",timestamp);
	XMLElement* parametre_element = doc->NewElement("timestamp");
	parametre_element->InsertFirstChild( doc->NewText(buffer));
	xmlframe_node->InsertEndChild(parametre_element);

	sprintf(buffer,"%d",frame.getFrameIndex());
	parametre_element = doc->NewElement("frame_index");
	parametre_element->InsertFirstChild( doc->NewText(buffer));
	xmlframe_node->InsertEndChild(parametre_element);

	XMLNode*  users_node = xmlframe_node->InsertEndChild( doc->NewElement( "users" ) );	
	
	for (int i = 0; i < users.getSize(); ++i){
		const nite::UserData& user = users[i];

		if(!user.isLost() && user.isVisible() && user.getSkeleton().getState() == nite::SKELETON_TRACKED){
			XMLNode* user_node = users_node->InsertEndChild( doc->NewElement( "user" ) );
			XMLElement* parametres[5] = { doc->NewElement( "id" ), doc->NewElement( "isNew" ), doc->NewElement( "isVisible" ),doc->NewElement( "isLost" ),
			doc->NewElement( "skeleton_State" ) };
			
			sprintf(buffer,"%d",user.getId());
			parametres[0]->InsertFirstChild( doc->NewText(buffer));
			sprintf(buffer,"%d",user.isNew());
			parametres[1]->InsertFirstChild( doc->NewText(buffer));
			sprintf(buffer,"%d",user.isVisible());
			parametres[2]->InsertFirstChild( doc->NewText(buffer));
			sprintf(buffer,"%d",user.isLost());			
			parametres[3]->InsertFirstChild( doc->NewText(buffer));
			sprintf(buffer,"%d",user.getSkeleton().getState());
			parametres[4]->InsertFirstChild( doc->NewText(buffer));

			user_node->InsertEndChild( parametres[0] );
			user_node->InsertEndChild( parametres[1] );
			user_node->InsertEndChild( parametres[2] );
			user_node->InsertEndChild( parametres[3] );
			user_node->InsertEndChild( parametres[4] );


			XMLNode* joints_node= user_node->InsertEndChild( doc->NewElement( "joints" ) );

			XMLNode* articulaciones[17] = {doc->NewElement("joint_head"),doc->NewElement("joint_neck"),doc->NewElement("joint_left_shoulder"),doc->NewElement("join_right_shoulder"),doc->NewElement("joint_left_elbow"),doc->NewElement("joint_right_elbow"),doc->NewElement("joint_left_hand"),doc->NewElement("joint_right_hand"),doc->NewElement("joint_torso"),doc->NewElement("joint_left_hip"),doc->NewElement("joint_right_hip"),doc->NewElement("joint_left_knee"),doc->NewElement("joint_right_knee"),doc->NewElement("joint_left_foot"),doc->NewElement("joint_right_foot"),doc->NewElement("center_of_mass"),doc->NewElement("bounding_box")};
		

			for(int j=0;j<15;j++){
				const nite::SkeletonJoint& joint=user.getSkeleton().getJoint((nite::JointType)j);

				XMLNode* position_node = articulaciones[j]->InsertEndChild( doc->NewElement( "position" ) );

				XMLElement* positionx_element = doc->NewElement("positionx");
				sprintf(buffer,"%f",joint.getPosition().x);
				positionx_element->InsertFirstChild( doc->NewText(buffer));
				position_node->InsertEndChild(positionx_element);

				XMLElement* positiony_element = doc->NewElement("positiony");
				sprintf(buffer,"%f",joint.getPosition().y);
				positiony_element->InsertFirstChild( doc->NewText(buffer));
				position_node->InsertEndChild(positiony_element);

				XMLElement* positionz_element = doc->NewElement("positionz");
				sprintf(buffer,"%f",joint.getPosition().z);
				positionz_element->InsertFirstChild( doc->NewText(buffer));
				position_node->InsertEndChild(positionz_element);
			
				XMLElement* position_confidence_element = doc->NewElement("position_confidence");
				sprintf(buffer,"%f",joint.getPositionConfidence());
				position_confidence_element->InsertFirstChild( doc->NewText(buffer));
				position_node->InsertEndChild(position_confidence_element);


				XMLNode* orientation_node = articulaciones[j]->InsertEndChild( doc->NewElement( "orientation" ) );

				XMLElement* orientationx_element = doc->NewElement("orientationx");
				sprintf(buffer,"%f",joint.getOrientation().x);
				orientationx_element->InsertFirstChild( doc->NewText(buffer));
				orientation_node->InsertEndChild(orientationx_element);

				XMLElement* orientationy_element = doc->NewElement("orientationy");
				sprintf(buffer,"%f",joint.getOrientation().y);
				orientationy_element->InsertFirstChild( doc->NewText(buffer));
				orientation_node->InsertEndChild(orientationy_element);

				XMLElement* orientationz_element = doc->NewElement("orientationz");
				sprintf(buffer,"%f",joint.getOrientation().z);
				orientationz_element->InsertFirstChild( doc->NewText(buffer));
				orientation_node->InsertEndChild(orientationz_element);

				XMLElement* orientationw_element = doc->NewElement("orientationw");
				sprintf(buffer,"%f",joint.getOrientation().w);
				orientationw_element->InsertFirstChild( doc->NewText(buffer));
				orientation_node->InsertEndChild(orientationw_element);
				
				XMLElement* orientation_confidence_element = doc->NewElement("orientation_confidence");
				sprintf(buffer,"%f",joint.getOrientationConfidence());
				orientation_confidence_element->InsertFirstChild( doc->NewText(buffer));
				orientation_node->InsertEndChild(orientation_confidence_element);
			}

			XMLNode* position_node = articulaciones[15]->InsertEndChild( doc->NewElement( "position" ) );

			XMLElement* positionx_element = doc->NewElement("positionx");
			sprintf(buffer,"%f",user.getCenterOfMass().x);
			positionx_element->InsertFirstChild( doc->NewText(buffer));
			position_node->InsertEndChild(positionx_element);

			XMLElement* positiony_element = doc->NewElement("positiony");
			sprintf(buffer,"%f",user.getCenterOfMass().y);
			positiony_element->InsertFirstChild( doc->NewText(buffer));
			position_node->InsertEndChild(positiony_element);

			XMLElement* positionz_element = doc->NewElement("positionz");
			sprintf(buffer,"%f",user.getCenterOfMass().z);
			positionz_element->InsertFirstChild( doc->NewText(buffer));
			position_node->InsertEndChild(positionz_element);


			XMLNode* bb_position_node = articulaciones[16]->InsertEndChild( doc->NewElement( "position" ) );

			XMLElement* bb_max_x_element = doc->NewElement("max_x");
			sprintf(buffer,"%f",user.getBoundingBox().max.x);
			bb_max_x_element->InsertFirstChild( doc->NewText(buffer));
			bb_position_node->InsertEndChild(bb_max_x_element);

			XMLElement* bb_max_y_element = doc->NewElement("max_y");
			sprintf(buffer,"%f",user.getBoundingBox().max.y);
			bb_max_y_element->InsertFirstChild( doc->NewText(buffer));
			bb_position_node->InsertEndChild(bb_max_y_element);

			XMLElement* bb_max_z_element = doc->NewElement("max_z");
			sprintf(buffer,"%f",user.getBoundingBox().max.z);
			bb_max_z_element->InsertFirstChild( doc->NewText(buffer));
			bb_position_node->InsertEndChild(bb_max_z_element);

			XMLElement* bb_min_x_element = doc->NewElement("min_x");
			sprintf(buffer,"%f",user.getBoundingBox().min.x);
			bb_min_x_element->InsertFirstChild( doc->NewText(buffer));
			bb_position_node->InsertEndChild(bb_min_x_element);

			XMLElement* bb_min_y_element = doc->NewElement("min_y");
			sprintf(buffer,"%f",user.getBoundingBox().min.y);
			bb_min_y_element->InsertFirstChild( doc->NewText(buffer));
			bb_position_node->InsertEndChild(bb_min_y_element);

			XMLElement* bb_min_z_element = doc->NewElement("min_z");
			sprintf(buffer,"%f",user.getBoundingBox().min.z);
			bb_min_z_element->InsertFirstChild( doc->NewText(buffer));
			bb_position_node->InsertEndChild(bb_min_z_element);
			
			for(int k=0; k<17; k++){
				joints_node->InsertEndChild(articulaciones[k]);
			}
		}
	}

	doc->SaveFile( xml_filename, true );
	delete doc;
}

void DibujarEsqueleto(nite::UserTracker* pUserTracker, const nite::Array<nite::UserData>& users,IplImage bgrIpl)
{
	float coordinates[6] = {0};
	
	for (int i = 0; i < users.getSize(); ++i){
		const nite::UserData& user = users[i];

		if(!user.isLost() && user.isVisible() && user.getSkeleton().getState() == nite::SKELETON_TRACKED){
			int c[]={255,255/user.getId(),255/user.getId()};	
			const nite::SkeletonJoint& head=user.getSkeleton().getJoint(nite::JOINT_HEAD);
			const nite::SkeletonJoint& neck=user.getSkeleton().getJoint(nite::JOINT_NECK);
			const nite::SkeletonJoint& left_shoulder=user.getSkeleton().getJoint(nite::JOINT_LEFT_SHOULDER);
			const nite::SkeletonJoint& right_shoulder=user.getSkeleton().getJoint(nite::JOINT_RIGHT_SHOULDER);
			const nite::SkeletonJoint& left_elbow=user.getSkeleton().getJoint(nite::JOINT_LEFT_ELBOW);
			const nite::SkeletonJoint& right_elbow=user.getSkeleton().getJoint(nite::JOINT_RIGHT_ELBOW);
			const nite::SkeletonJoint& left_hand=user.getSkeleton().getJoint(nite::JOINT_LEFT_HAND);
			const nite::SkeletonJoint& right_hand=user.getSkeleton().getJoint(nite::JOINT_RIGHT_HAND);
			const nite::SkeletonJoint& torso=user.getSkeleton().getJoint(nite::JOINT_TORSO);
			const nite::SkeletonJoint& left_hip=user.getSkeleton().getJoint(nite::JOINT_LEFT_HIP);
			const nite::SkeletonJoint& right_hip=user.getSkeleton().getJoint(nite::JOINT_RIGHT_HIP);
			const nite::SkeletonJoint& left_knee=user.getSkeleton().getJoint(nite::JOINT_LEFT_KNEE);
			const nite::SkeletonJoint& right_knee=user.getSkeleton().getJoint(nite::JOINT_RIGHT_KNEE);
			const nite::SkeletonJoint& left_foot=user.getSkeleton().getJoint(nite::JOINT_LEFT_FOOT);
			const nite::SkeletonJoint& right_foot=user.getSkeleton().getJoint(nite::JOINT_RIGHT_FOOT);
			
	
			//1
			pUserTracker->convertJointCoordinatesToDepth(head.getPosition().x, head.getPosition().y, head.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(neck.getPosition().x, neck.getPosition().y, neck.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//2
			pUserTracker->convertJointCoordinatesToDepth(left_shoulder.getPosition().x, left_shoulder.getPosition().y, left_shoulder.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(left_elbow.getPosition().x, left_elbow.getPosition().y, left_elbow.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );
		
			//3
			pUserTracker->convertJointCoordinatesToDepth(left_elbow.getPosition().x, left_elbow.getPosition().y, left_elbow.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(left_hand.getPosition().x, left_hand.getPosition().y, left_hand.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//4	
			pUserTracker->convertJointCoordinatesToDepth(right_shoulder.getPosition().x, right_shoulder.getPosition().y, right_shoulder.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(right_elbow.getPosition().x, right_elbow.getPosition().y, right_elbow.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );
			
			//5
			pUserTracker->convertJointCoordinatesToDepth(right_elbow.getPosition().x, right_elbow.getPosition().y, right_elbow.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(right_hand.getPosition().x, right_hand.getPosition().y, right_hand.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//6
			pUserTracker->convertJointCoordinatesToDepth(left_shoulder.getPosition().x, left_shoulder.getPosition().y, left_shoulder.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(right_shoulder.getPosition().x, right_shoulder.getPosition().y, right_shoulder.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//7
			pUserTracker->convertJointCoordinatesToDepth(left_shoulder.getPosition().x, left_shoulder.getPosition().y, left_shoulder.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(torso.getPosition().x, torso.getPosition().y, torso.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//8
			pUserTracker->convertJointCoordinatesToDepth(right_shoulder.getPosition().x, right_shoulder.getPosition().y, right_shoulder.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(torso.getPosition().x, torso.getPosition().y, torso.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//9
			pUserTracker->convertJointCoordinatesToDepth(torso.getPosition().x, torso.getPosition().y, torso.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(left_hip.getPosition().x, left_hip.getPosition().y, left_hip.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//10
			pUserTracker->convertJointCoordinatesToDepth(torso.getPosition().x, torso.getPosition().y, torso.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(right_hip.getPosition().x, right_hip.getPosition().y, right_hip.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//11
			pUserTracker->convertJointCoordinatesToDepth(left_hip.getPosition().x, left_hip.getPosition().y, left_hip.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(right_hip.getPosition().x, right_hip.getPosition().y, right_hip.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//12
			pUserTracker->convertJointCoordinatesToDepth(left_hip.getPosition().x, left_hip.getPosition().y, left_hip.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(left_knee.getPosition().x, left_knee.getPosition().y, left_knee.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//13
			pUserTracker->convertJointCoordinatesToDepth(left_knee.getPosition().x, left_knee.getPosition().y, left_knee.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(left_foot.getPosition().x, left_foot.getPosition().y, left_foot.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//14
			pUserTracker->convertJointCoordinatesToDepth(right_hip.getPosition().x, right_hip.getPosition().y, right_hip.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(right_knee.getPosition().x, right_knee.getPosition().y, right_knee.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );

			//15
			pUserTracker->convertJointCoordinatesToDepth(right_knee.getPosition().x, right_knee.getPosition().y, right_knee.getPosition().z, &coordinates[0], &coordinates[1]);
			pUserTracker->convertJointCoordinatesToDepth(right_foot.getPosition().x, right_foot.getPosition().y, right_foot.getPosition().z, &coordinates[3], &coordinates[4]);
			cvLine(&bgrIpl, cvPoint(coordinates[0],coordinates[1]), cvPoint(coordinates[3],coordinates[4]), CV_RGB(c[0],c[1],c[2]), 5, 8, 0 );
			
		}
	}
}


void save_depth(char *filepath,char *filename,openni::VideoFrameRef depthFrame ,openni::RGB888Pixel* pTexRow,unsigned int m_nTexMapX,openni::RGB888Pixel* m_pTexMap, nite::UserTrackerFrameRef userTrackerFrame,nite::UserTracker* pUserTracker){
	
	try {
		char buffer[500] = "";
		char buffer2[500] = "";
		sprintf(buffer,"/%s/%s/depth/%s_%d_%lu_depth.jpg",filepath,filename,filename,depthFrame.getFrameIndex(),depthFrame.getTimestamp());
		sprintf(buffer2,"/%s/%s/esqueleto/%s_%d_%lu_esqueleto.jpg",filepath,filename,filename,depthFrame.getFrameIndex(),depthFrame.getTimestamp());		

		Mat colorImage; 
		Mat colorArr[3]; 
		colorArr[0] = Mat(depthFrame.getVideoMode().getResolutionY(),depthFrame.getVideoMode().getResolutionX(),CV_8U); 
		colorArr[1] = Mat(depthFrame.getVideoMode().getResolutionY(),depthFrame.getVideoMode().getResolutionX(),CV_8U); 
		colorArr[2] = Mat(depthFrame.getVideoMode().getResolutionY(),depthFrame.getVideoMode().getResolutionX(),CV_8U); 		

		Mat esqueletoImage; 
		Mat esqueletoArr[3]; 
		esqueletoArr[0] = Mat(depthFrame.getVideoMode().getResolutionY(),depthFrame.getVideoMode().getResolutionX(),CV_8U); 
		esqueletoArr[1] = Mat(depthFrame.getVideoMode().getResolutionY(),depthFrame.getVideoMode().getResolutionX(),CV_8U); 
		esqueletoArr[2] = Mat(depthFrame.getVideoMode().getResolutionY(),depthFrame.getVideoMode().getResolutionX(),CV_8U); 		

		if(userTrackerFrame.getUsers().getSize() > 0){
			char xml_filename[500]="";
			sprintf(xml_filename,"/%s/%s/depth/xml/%s_%d_%lu_depth.xml",filepath,filename,filename,depthFrame.getFrameIndex(),depthFrame.getTimestamp());
			const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();

			set_metadatos(xml_filename,users,depthFrame,userTrackerFrame.getTimestamp());
		}

		for (int y = 0; y < depthFrame.getHeight(); ++y){
			openni::RGB888Pixel* pPixel = pTexRow + depthFrame.getCropOriginX();
			uchar* Bptr = colorArr[0].ptr<uchar>(y); 
        		uchar* Gptr = colorArr[1].ptr<uchar>(y); 
        		uchar* Rptr = colorArr[2].ptr<uchar>(y); 

			uchar* Bptr2 = esqueletoArr[0].ptr<uchar>(y); 
        		uchar* Gptr2= esqueletoArr[1].ptr<uchar>(y); 
        		uchar* Rptr2 = esqueletoArr[2].ptr<uchar>(y); 

			for (int x = 0; x < depthFrame.getWidth(); ++x,++pPixel){
				Bptr[x] = pPixel->b; 
                		Gptr[x] = pPixel->g; 
                		Rptr[x] = pPixel->r;
				Bptr2[x] = 0; 
                		Gptr2[x] = 0; 
                		Rptr2[x] = 0;  
			}
			pTexRow += m_nTexMapX;
		} 
		merge(colorArr,3,colorImage); 
		IplImage bgrIpl = colorImage;    
		cvSaveImage(buffer,&bgrIpl);
		

		merge(esqueletoArr,3,esqueletoImage); 
		bgrIpl = esqueletoImage;    
		DibujarEsqueleto(pUserTracker, userTrackerFrame.getUsers(),bgrIpl);
		cvSaveImage(buffer2,&bgrIpl);

		delete[] m_pTexMap;

	}catch (std::exception& e) {
		printf("Expection in save_depth(): %s\n",e.what());
	}
}

void Recorder::Display()
{
	const openni::RGB888Pixel* pPixel_rgb=0;
	if (color.readFrame(&colorFrame) == openni::STATUS_OK)
	{	
		const openni::RGB888Pixel* imageBuffer = (const openni::RGB888Pixel*)colorFrame.getData();
		pPixel_rgb = imageBuffer;
	}

	nite::UserTrackerFrameRef userTrackerFrame;
	openni::VideoFrameRef depthFrame;

	nite::Status rc = m_pUserTracker->readFrame(&userTrackerFrame);
	if (rc != nite::STATUS_OK)
	{
		printf("GetNextData failed\n");
		return;
	}
	depthFrame = userTrackerFrame.getDepthFrame();

	m_nTexMapX = MIN_CHUNKS_SIZE(depthFrame.getVideoMode().getResolutionX(), TEXTURE_SIZE);
	m_nTexMapY = MIN_CHUNKS_SIZE(depthFrame.getVideoMode().getResolutionY(), TEXTURE_SIZE);
	m_pTexMap = new openni::RGB888Pixel[m_nTexMapX * m_nTexMapY];
	

	const nite::UserMap& userLabels = userTrackerFrame.getUserMap();

	if(mostrar_video){
		glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glOrtho(0, GL_WIN_SIZE_X, GL_WIN_SIZE_Y, 0, -1.0, 1.0);
	}

	if (depthFrame.isValid() && g_drawDepth){
		calculateHistogram(m_pDepthHist, MAX_DEPTH, depthFrame);
	}

	memset(m_pTexMap, 0, m_nTexMapX*m_nTexMapY*sizeof(openni::RGB888Pixel));
	float factor[3] = {1, 1, 1};

	if (depthFrame.isValid() && g_drawDepth){
		const nite::UserId* pLabels = userLabels.getPixels();
		const openni::DepthPixel* pDepthRow = (const openni::DepthPixel*)depthFrame.getData();
		openni::RGB888Pixel* pTexRow = m_pTexMap + depthFrame.getCropOriginY() * m_nTexMapX;
		int rowSize = depthFrame.getStrideInBytes() / sizeof(openni::DepthPixel);

		for (int y = 0; y < depthFrame.getHeight(); ++y){
			const openni::DepthPixel* pDepth = pDepthRow;
			openni::RGB888Pixel* pTex = pTexRow + depthFrame.getCropOriginX();

			for (int x = 0; x < depthFrame.getWidth(); ++x, ++pDepth, ++pTex, ++pLabels)
			{
				if (*pDepth != 0 )
				{
					if (*pLabels == 0)
					{
						if (!g_drawBackground)
						{
							factor[0] = factor[1] = factor[2] = 0;

						}
						else
						{
							factor[0] = Colors[colorCount][0];
							factor[1] = Colors[colorCount][1];
							factor[2] = Colors[colorCount][2];
						}
					}
					else
					{
						factor[0] = Colors[*pLabels % colorCount][0];
						factor[1] = Colors[*pLabels % colorCount][1];
						factor[2] = Colors[*pLabels % colorCount][2];
					}

					int nHistValue = m_pDepthHist[*pDepth];
					pTex->r = nHistValue*factor[0];
					pTex->g = nHistValue*factor[1];
					pTex->b = nHistValue*factor[2];


					factor[0] = factor[1] = factor[2] = 1;
				}
			}

			pDepthRow += rowSize;
			pTexRow += m_nTexMapX;
		}
	}

	if(mostrar_video){
		

		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_nTexMapX, m_nTexMapY, 0, GL_RGB, GL_UNSIGNED_BYTE, m_pTexMap);

		glColor4f(1,1,1,1);

		glEnable(GL_TEXTURE_2D);
		glBegin(GL_QUADS);

		g_nXRes = depthFrame.getVideoMode().getResolutionX();
		g_nYRes = depthFrame.getVideoMode().getResolutionY();

		glTexCoord2f(0, 0);
		glVertex2f(0, 0);

		glTexCoord2f((float)g_nXRes/(float)m_nTexMapX, 0);
		glVertex2f(GL_WIN_SIZE_X, 0);

		glTexCoord2f((float)g_nXRes/(float)m_nTexMapX, (float)g_nYRes/(float)m_nTexMapY);
		glVertex2f(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);

		glTexCoord2f(0, (float)g_nYRes/(float)m_nTexMapY);
		glVertex2f(0, GL_WIN_SIZE_Y);

		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	const nite::Array<nite::UserData>& users = userTrackerFrame.getUsers();
	for (int i = 0; i < users.getSize(); ++i)
	{
		const nite::UserData& user = users[i];

		updateUserState(user, userTrackerFrame.getTimestamp());
		if (user.isNew()){
			m_pUserTracker->startSkeletonTracking(user.getId());
		}else if (!user.isLost() && mostrar_video){
			if (g_drawStatusLabel){
				DrawStatusLabel(m_pUserTracker, user);
			}
			if (g_drawCenterOfMass){
				DrawCenterOfMass(m_pUserTracker, user);
			}
			if (g_drawBoundingBox){
				DrawBoundingBox(user);
			}
			if (users[i].getSkeleton().getState() == nite::SKELETON_TRACKED && g_drawSkeleton){
				DrawSkeleton(m_pUserTracker, user);
			}
		}
	}

	if(mostrar_video){
		if (g_drawFrameId){
			DrawFrameId(userTrackerFrame.getFrameIndex());
		}

		if (g_generalMessage[0] != '\0'){
			char *msg = g_generalMessage;
			glColor3f(1.0f, 0.0f, 0.0f);
			glRasterPos2i(100, 20);
			glPrintString(GLUT_BITMAP_HELVETICA_18, msg);
		}
		glutSwapBuffers();
	}

	if(capturar && capturar_rgb && colorFrame.isValid()){
		hilos.push_back(thread (save_rgb,filepath,filename,colorFrame,pPixel_rgb));
	}

	if(capturar && capturar_depth && depthFrame.isValid()){
		openni::RGB888Pixel* pTexRow = m_pTexMap + depthFrame.getCropOriginY() * m_nTexMapX;
		hilos.push_back(thread (save_depth,filepath,filename,depthFrame,pTexRow,m_nTexMapX,m_pTexMap,userTrackerFrame,m_pUserTracker));
	}else{
		delete[] m_pTexMap;
	}
} 

void Recorder::OnKey(unsigned char key, int /*x*/, int /*y*/)
{
	switch (key)
	{
	case 27:
		while(!hilos.empty()){
			auto& t= hilos.front();
			if(t.joinable()){			
				t.join();
			}
			hilos.pop_front();
		}
		Finalize();
		exit (EXIT_SUCCESS);
	case 's':
		g_drawSkeleton = !g_drawSkeleton;
		printf("\nDraw skeleton: %d\n",g_drawSkeleton);
		break;
	case 'l':
		g_drawStatusLabel = !g_drawStatusLabel;
		printf("\nDraw status level: %d\n",g_drawStatusLabel);
		break;
	case 'm':
		g_drawCenterOfMass = !g_drawCenterOfMass;
		printf("\nDraw center of mass: %d\n",g_drawCenterOfMass);
		break;
	case 'x':
		g_drawBoundingBox = !g_drawBoundingBox;
		printf("\nDraw bounding box: %d\n",g_drawBoundingBox);
		break;
	case 'b':
		g_drawBackground = !g_drawBackground;
		printf("\nDraw background: %d\n",g_drawBackground);
		break;
	case 'd':
		g_drawDepth = !g_drawDepth;
		printf("\nDraw depth: %d\n",g_drawDepth);
		break;
	case 'c':
		capturar = !capturar;
		printf("\nCapture frames: %d ==> RGB:%d, Depth:%d.\n",capturar,capturar_rgb,capturar_depth);
		break;
	case 'r':
		capturar_rgb = !capturar_rgb;
		printf("\nCapture RGB:%d.\n",capturar_rgb);
		break;
	case 'p':
		capturar_depth = !capturar_depth;
		printf("\nCapture Depth:%d.\n",capturar_depth);
		break;
	case 'f':
		g_drawFrameId = !g_drawFrameId;
		printf("\nDraw Frame Id: %d\n",g_drawFrameId);
		break;
	case 'i':
		mostrar_video = !mostrar_video;
		printf("\nShow video: %d.\n",mostrar_video);
		break;
	}

}

openni::Status Recorder::InitOpenGL(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);
	glutCreateWindow (m_strSampleName);
	glutSetCursor(GLUT_CURSOR_NONE);

	InitOpenGLHooks();

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	glEnableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	return openni::STATUS_OK;

}
void Recorder::InitOpenGLHooks()
{
	glutKeyboardFunc(glutKeyboard);
	glutDisplayFunc(glutDisplay);
	glutIdleFunc(glutIdle);
}
