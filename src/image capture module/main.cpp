#include "Recorder.h"

int main(int argc, char** argv)
{
	openni::Status rc = openni::STATUS_OK;

	Recorder recorder("Recorder");

	rc = recorder.Init(argc, argv);
	if (rc != openni::STATUS_OK)
	{
		return 1;
	}
	recorder.Run();
}
