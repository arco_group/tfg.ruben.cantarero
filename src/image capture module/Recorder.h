#ifndef _NITE_USER_VIEWER_H_
#define _NITE_USER_VIEWER_H_

#include "NiTE.h"
#include <iostream>
#include <fstream>
#include <thread>
#include <list>
#include "cv.h"
#include <highgui.h>
#include "tinyxml2.h"


#define MAX_DEPTH 10000

class Recorder
{
public:
	Recorder(const char* strSampleName);
	virtual ~Recorder();

	virtual openni::Status Init(int argc, char **argv);
	virtual openni::Status Run();	

protected:
	virtual void Display();
	virtual void DisplayPostDraw(){};

	virtual void OnKey(unsigned char key, int x, int y);

	virtual openni::Status InitOpenGL(int argc, char **argv);
	void InitOpenGLHooks();

	void Finalize();

private:
	Recorder(const Recorder&);
	Recorder& operator=(Recorder&);

	static Recorder* ms_self;
	static void glutIdle();
	static void glutDisplay();
	static void glutKeyboard(unsigned char key, int x, int y);

	float				m_pDepthHist[MAX_DEPTH];
	char			m_strSampleName[ONI_MAX_STR];
	openni::RGB888Pixel*		m_pTexMap;
	unsigned int		m_nTexMapX;
	unsigned int		m_nTexMapY;

	openni::Device		m_device;
	nite::UserTracker* m_pUserTracker;

	nite::UserId m_poseUser;
	uint64_t m_poseTime;
};


#endif // _NITE_USER_VIEWER_H_
