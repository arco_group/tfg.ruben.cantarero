# Descripción #

Este repositorio alberga todo el contenido elaborado en el Trabajo Fin de Grado «KinBehR: KINect for human BEHaviour Recognition». En este proyecto se ha desarrollado un algoritmo de «aprendizaje máquina» (machine learning) que combina el análisis de imágenes de intensidad y profundidad capturadas con Kinect con una base de conocimiento donde se contenga información de alto nivel sobre acciones humanas. El sistema de reconocimiento estará basado en un clasificador de máquina de vectores de soporte o Support Vector Machines (SVM).

# Estructura #

El contenido del repositorio está dividido en dos carpetas:

* «doc»: Almacena todo el contenido relacionado con la documentación del proyecto. Se encuentra dividida a su vez en tres carpetas: «diagrams», que contiene los diagramas generados con la herramienta Dia; «graphics», que contiene los archivos empleados para la generación de las gráficas; y «memoria», que contiene los archivos latex de la memoria del proyecto. 
* «src»: Carpeta que contiene todos los archivos fuente clasificados a su vez en subcarpetas según al módulo al que pertenecen.

A continuación se describen las subcarpetas contenidas en la carpeta «src»:

* «actions recognition module»: contiene el sistema encargado de realizar el proceso de reconocimiento de acciones. Para más información sobre como se estructura véase la memoria.
* «graphic interface»: contiene el código fuente correspondiente a la interfaz gráfica del sistema.
* «image capture module»: contiene el código fuente correspondiente al módulo encargado de capturar las imágenes a través del dispositivo Kinect.
* «scripts»: contiene los scripts empleados para automatizar algunas acciones realizadas durante el proyecto. «confusion_matriz.pl» genera las matrices de confusión a partir de los resultados obtenidos en la clasificación. «frame_to_video.sh» monta los vídeos correspondientes a partir de una secuencia de imágenes.


# Instalación #

## Memoria ##
Para generar el archivo pdf correspondiente a la memoria es necesario instalar el paquete [esi-tfg](https://bitbucket.org/arco_group/esi-tfg).

## Módulo de reconocimiento de acciones ##

### Configuración de las fuentes de paquetes multimedia ###

La mayoría de las aplicaciones que estamos a punto de instalar se puede instalar a través de apt-get, pero para hacer eso tenemos que asegurarnos de que las fuentes de los paquetes multimedia están disponibles:

1). Edite el archivo en el que se enumeran las fuentes de donde los paquetes se pueden recuperar:

```
#!c++

$ sudo gedit /etc/apt/sources.list
```

2). Añada la siguiente línea al archivo:

```
#!c++

deb http://www.debian-multimedia.org sid main non-free
```

3). Actualización de la lista disponible de paquetes: 

```
#!c++

$ sudo apt-get update
```
### Instalación opencv, ffmpeg y vlc ###

Ambos paquetes deben estar disponibles, por lo que una búsqueda rápida se debe dar el nombre de la última versión del paquete:

4). Busque ffmepg y vlc:


```
#!c++

$ apt-cache search opencv
libcv-dev - development files for libcv 
libcv2.1 - computer vision library 
libcvaux-dev - development files for libcvaux 
libcvaux2.1 - computer vision extension library 
libhighgui-dev - development files for libhighgui 
libhighgui2.1 - computer vision GUI library 
opencv-doc - OpenCV documentation and examples 
python-opencv - Python bindings for the computer vision library 
libcv1 - computer vision library 
libcvaux1 - computer vision extension library 
libhighgui1 - computer vision GUI library

$ apt-cache search ffmepg
[…]
ffmpeg - audio/video encoder, streaming server & audio/video file onverter
ffmpeg-dbg - Debug symbols for FFmpeg related packages. 
ffmpeg-doc - Documentation of the FFmpeg API.
[…]

$ apt-cache search vlc
[…]
vlc - multimedia player and streamer 
vlc-data - Common data for VLC 
vlc-dbg - debugging symbols for vlc 
vlc-nox - multimedia player and streamer (without X support)
[...]
```

5). Instale los paquetes: 


```
#!c++

$ sudo apt-get install libcv-dev libcv2.1 ffmpeg vlc libcvaux2.1
```


### Instalación de ANN, SVM y STIP ###

6). Inicie la instalación de la aplicación [ann_1.1.2](http://www.cs.umd.edu/~mount/ANN/).

```
#!c++

~/BoW/sources$ cd ann_1.1.2
~/BoW/sources/ann_1.1.2$ make realclean
[..]
~/BoW/sources/ann_1.1.2$ make linux-g++
[..]
~/BoW/sources/ann_1.1.2$ sudo cp bin/ann* /usr/local/sbin
```

7). Continúe con la instalación de [bsvm-2.06](http://www.csie.ntu.edu.tw/~cjlin/bsvm/).

```
#!c++

~/BoW/sources/ann_1.1.2$ cd ../bsvm-2.06
~/BoW/sources/bsvm-2.06$ make clean
~/BoW/sources/bsvm-2.06$ make all
~/BoW/sources/bsvm-2.06$ sudo cp bsvm-train bsvm-predit /usr/local/sbin
```

8). Finalmente instale la aplicación [stip-1.1-winlinux](http://www.di.ens.fr/~laptev/download.html)

```
#!c++

~/BoW/sources/bsvm-2.06$ cd ../stip-1.1-winlinux/bin
~/BoW/sources/stip-1.1-winlinux/bin$ chmod a+x stipdet stipshow
~/BoW/sources/stip-1.1-winlinux/bin$ sudo cp stipdet /usr/local/sbin
~/BoW/sources/stip-1.1-winlinux/bin$ sudo cp stipshow /usr/local/sbin 
```

9). Hay algunos problemas con la aplicación stip, pero esto se puede resolver fácilmente mediante la ejecución de los siguientes comandos:

```
#!c++

~/BoW/sources/bsvm-2.06$ cd /usr/lib
/usr/lib$ sudo sudo ln -s libcxcore.so.2.0.0 libcxcore.so.1
/usr/lib$ sudo ln -s libcv.so.2.0.0 libcv.so.1 
/usr/lib$ sudo ln -s libhighgui.so.2.0.0 libhighgui.so.1 
/usr/lib$ sudo ln -s libcvaux.so.2.0.0 libcvaux.so.1 
/usr/lib$ sudo ln -s libml.so.2.0.0 libml.so.1
```

## Interfaz gráfica ##

Es necesario instalar los paquetes de las librerías de [Qt](http://qt-project.org/).

## Módulo de captura de imágenes ## 

Es necesario instalar los siguientes elementos:

* OpenNI (Página Web ya no disponible)
* NiTE (Página Web ya no disponible)
* SensorKinect [descarga](https://github.com/avin2/SensorKinect)
* OpenNI2-FreenectDriver-master [descarga](https://github.com/OpenKinect/libfreenect/tree/master/OpenNI2-FreenectDriver)

# Ejecución #

## Memoria ##

La ejecución de la memoria se lleva a cabo a través del archivo Makefile.

```
#!c++

$make
```

## Módulo de reconocimiento de acciones ##

El módulo de reconocimiento de acciones se arranca con el siguiente comando:

```
#!c++

~/BoW$ sh run_all.sh &> result
```

Para más información acerca de su funcionamiento véase la memoria.

## Interfaz gráfica ##

La compilación de la interfaz gráfica se lleva a cabo a través del archivo Makefile.

```
#!c++

$make
```

Para la ejecución de la aplicación emplee el siguiente comando:

```
#!c++

~/kinbehr/KinbehrGUI/BUILD$ ./KinbehrGUI
```

Capturas de pantalla:

![GUI_1.png](https://bitbucket.org/repo/j75Ezq/images/1490885321-GUI_1.png)
![GUI_2.png](https://bitbucket.org/repo/j75Ezq/images/2522272333-GUI_2.png)
![GUI_3.png](https://bitbucket.org/repo/j75Ezq/images/3573749975-GUI_3.png)
![GUI_4.png](https://bitbucket.org/repo/j75Ezq/images/3625552071-GUI_4.png)

## Módulo de captura de imágenes ##

La compilación del módulo de captura de imágenes se lleva a cabo a través del archivo Makefile.

```
#!c++

$make
```

Para la ejecución de la aplicación emplee el siguiente comando:

```
#!c++

~/src/image_capture_module/Bin/x[32/64]-Release/$ ./Recorder [directorio destino] [nombre de las capturas]
```

Atajos de teclado de la aplicación:

* Esc: salir de la aplicación.
* s: dibujar esqueleto.
* l: dibujar etiqueta de estado.
* m: dibujar centro de masa.
* x: dibujar límites de usuario.
* b: dibujar fondo.
* d: dibujar profundidad.
* c: comenzar a capturar frames.
* r: capturar flujo RGB.
* p: capturar flujo de profundidad.
* f: dibujar id del frame.
* i: mostrar vídeo.


Capturas de pantalla:

![modulo_captura_3.png](https://bitbucket.org/repo/j75Ezq/images/1048528378-modulo_captura_3.png)
![modulo_captura_5.png](https://bitbucket.org/repo/j75Ezq/images/2414051314-modulo_captura_5.png)
![modulo_captura_4.png](https://bitbucket.org/repo/j75Ezq/images/1265146640-modulo_captura_4.png)
![modulo_captura_6.png](https://bitbucket.org/repo/j75Ezq/images/1869786665-modulo_captura_6.png)