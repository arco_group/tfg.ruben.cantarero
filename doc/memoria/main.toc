\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {v}}{chapter*.1}
\contentsline {chapter}{Abstract}{\es@scroman {vii}}{chapter*.3}
\contentsline {chapter}{Agradecimientos}{\es@scroman {ix}}{chapter*.4}
\contentsline {chapter}{\'Indice general}{\es@scroman {xiii}}{chapter*.5}
\contentsline {chapter}{\'{I}ndice de cuadros}{\es@scroman {xix}}{chapter*.6}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {xxi}}{chapter*.7}
\contentsline {chapter}{\IeC {\'I}ndice de listados}{\es@scroman {xxv}}{chapter*.8}
\contentsline {chapter}{Listado de acr\IeC {\'o}nimos}{\es@scroman {xxvii}}{chapter*.9}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Estructura del documento}{4}{section.1.1}
\contentsline {chapter}{\numberline {2}Objetivos}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Objetivo general}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Objetivos espec\IeC {\'\i }ficos}{5}{subsection.2.1.1}
\contentsline {chapter}{\numberline {3}Objectives}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}General objective}{9}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Specific objectives}{9}{subsection.3.1.1}
\contentsline {chapter}{\numberline {4}Antecedentes}{13}{chapter.4}
\contentsline {section}{\numberline {4.1}Kinect para Xbox 360}{13}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Introducci\IeC {\'o}n}{13}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}La creaci\IeC {\'o}n de Kinect}{14}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}Precedentes}{15}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}Evoluci\IeC {\'o}n}{17}{subsubsection.4.1.2.2}
\contentsline {subsubsection}{\numberline {4.1.2.3}Homebrew de Kinect}{21}{subsubsection.4.1.2.3}
\contentsline {subsection}{\numberline {4.1.3}Desarrollo de aplicaciones alternativas}{24}{subsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.3.1}Ocio}{26}{subsubsection.4.1.3.1}
\contentsline {subsubsection}{\numberline {4.1.3.2}Medicina}{27}{subsubsection.4.1.3.2}
\contentsline {subsubsection}{\numberline {4.1.3.3}Educaci\IeC {\'o}n}{27}{subsubsection.4.1.3.3}
\contentsline {subsubsection}{\numberline {4.1.3.4}Accesibilidad}{28}{subsubsection.4.1.3.4}
\contentsline {subsection}{\numberline {4.1.4}Caracter\IeC {\'\i }sticas t\IeC {\'e}cnicas}{30}{subsection.4.1.4}
\contentsline {subsubsection}{\numberline {4.1.4.1}Segunda generaci\IeC {\'o}n de Kinect}{32}{subsubsection.4.1.4.1}
\contentsline {subsection}{\numberline {4.1.5}Ventajas de Kinect frente a otros dispositivos}{34}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Funcionamiento}{34}{subsection.4.1.6}
\contentsline {subsubsection}{\numberline {4.1.6.1}C\IeC {\'a}mara RGB}{34}{subsubsection.4.1.6.1}
\contentsline {subsubsection}{\numberline {4.1.6.2}C\IeC {\'a}mara profundidad}{35}{subsubsection.4.1.6.2}
\contentsline {subsubsection}{\numberline {4.1.6.3}Sistema de micr\IeC {\'o}fonos}{37}{subsubsection.4.1.6.3}
\contentsline {subsubsection}{\numberline {4.1.6.4}Motor de inclinaci\IeC {\'o}n}{37}{subsubsection.4.1.6.4}
\contentsline {subsection}{\numberline {4.1.7}\IeC {\textquestiondown }Por qu\IeC {\'e} Kinect?}{39}{subsection.4.1.7}
\contentsline {subsection}{\numberline {4.1.8}Librer\IeC {\'\i }as Kinect}{39}{subsection.4.1.8}
\contentsline {subsubsection}{\numberline {4.1.8.1}SDK Windows}{40}{subsubsection.4.1.8.1}
\contentsline {paragraph}{\numberline {4.1.8.1.1}Descripci\IeC {\'o}n del \acs {SDK}\newline }{40}{paragraph.4.1.8.1.1}
\contentsline {subparagraph}{Plataformas soportadas\newline }{45}{figure.4.35}
\contentsline {paragraph}{\numberline {4.1.8.1.2}Evoluci\IeC {\'o}n del framework\newline }{45}{paragraph.4.1.8.1.2}
\contentsline {paragraph}{\numberline {4.1.8.1.3}Ventajas y desventajas\newline }{46}{paragraph.4.1.8.1.3}
\contentsline {subparagraph}{Ventajas\newline }{46}{paragraph.4.1.8.1.3}
\contentsline {subparagraph}{Desventajas\newline }{47}{paragraph.4.1.8.1.3}
\contentsline {subsubsection}{\numberline {4.1.8.2}OpenNI}{47}{subsubsection.4.1.8.2}
\contentsline {paragraph}{\numberline {4.1.8.2.1}Descripci\IeC {\'o}n del framework\newline }{48}{paragraph.4.1.8.2.1}
\contentsline {subparagraph}{Plataformas soportadas\newline }{50}{figure.4.39}
\contentsline {subparagraph}{NiTE y otros middlewares de inter\IeC {\'e}s\newline }{51}{figure.4.39}
\contentsline {paragraph}{\numberline {4.1.8.2.2}Evoluci\IeC {\'o}n del framework\newline }{53}{paragraph.4.1.8.2.2}
\contentsline {paragraph}{\numberline {4.1.8.2.3}Ventajas y desventajas\newline }{55}{paragraph.4.1.8.2.3}
\contentsline {subparagraph}{Ventajas\newline }{55}{paragraph.4.1.8.2.3}
\contentsline {subparagraph}{Desventajas\newline }{56}{paragraph.4.1.8.2.3}
\contentsline {subsubsection}{\numberline {4.1.8.3}OpenKinect}{56}{subsubsection.4.1.8.3}
\contentsline {paragraph}{\numberline {4.1.8.3.1}Descripci\IeC {\'o}n del framework\newline }{56}{paragraph.4.1.8.3.1}
\contentsline {paragraph}{\numberline {4.1.8.3.2}Ventajas y desventajas\newline }{57}{paragraph.4.1.8.3.2}
\contentsline {subparagraph}{Ventajas\newline }{57}{paragraph.4.1.8.3.2}
\contentsline {subparagraph}{Desventajas\newline }{57}{paragraph.4.1.8.3.2}
\contentsline {section}{\numberline {4.2}Visi\IeC {\'o}n por computador}{58}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Perspectiva de visi\IeC {\'o}n por computador}{59}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Algoritmos de reconocimiento de acciones basados en v\IeC {\'\i }deo}{60}{subsection.4.2.2}
\contentsline {chapter}{\numberline {5}M\IeC {\'e}todo de trabajo}{65}{chapter.5}
\contentsline {section}{\numberline {5.1}Metodolog\IeC {\'\i }a de trabajo}{65}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Acerca de Scrum}{66}{subsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.1}Fases de Scrum}{67}{subsubsection.5.1.1.1}
\contentsline {subsubsection}{\numberline {5.1.1.2}Roles y responsabilidades}{68}{subsubsection.5.1.1.2}
\contentsline {subsubsection}{\numberline {5.1.1.3}Pr\IeC {\'a}cticas y conceptos relevantes}{69}{subsubsection.5.1.1.3}
\contentsline {subsection}{\numberline {5.1.2}Descripci\IeC {\'o}n de la planificaci\IeC {\'o}n}{71}{subsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.2.1}Fase 1: Pre-game}{72}{subsubsection.5.1.2.1}
\contentsline {subsubsection}{\numberline {5.1.2.2}Fase 2: Development}{73}{subsubsection.5.1.2.2}
\contentsline {paragraph}{\numberline {5.1.2.2.1}Iteraci\IeC {\'o}n 0 \newline }{73}{paragraph.5.1.2.2.1}
\contentsline {paragraph}{\numberline {5.1.2.2.2}Iteraci\IeC {\'o}n 1 \newline }{74}{paragraph.5.1.2.2.2}
\contentsline {paragraph}{\numberline {5.1.2.2.3}Iteraci\IeC {\'o}n 2 \newline }{75}{paragraph.5.1.2.2.3}
\contentsline {paragraph}{\numberline {5.1.2.2.4}Iteraci\IeC {\'o}n 3 \newline }{75}{paragraph.5.1.2.2.4}
\contentsline {paragraph}{\numberline {5.1.2.2.5}Iteraci\IeC {\'o}n 4 \newline }{76}{paragraph.5.1.2.2.5}
\contentsline {paragraph}{\numberline {5.1.2.2.6}Iteraci\IeC {\'o}n 5 \newline }{77}{paragraph.5.1.2.2.6}
\contentsline {subsubsection}{\numberline {5.1.2.3}Fase 3: Post-game}{78}{subsubsection.5.1.2.3}
\contentsline {section}{\numberline {5.2}Herramientas}{78}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Aplicaciones de desarrollo}{78}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Lenguajes de programaci\IeC {\'o}n}{80}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Documentaci\IeC {\'o}n y gr\IeC {\'a}ficos}{80}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Hardware}{80}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Sistemas operativos}{81}{subsection.5.2.5}
\contentsline {chapter}{\numberline {6}Desarrollo del proyecto}{83}{chapter.6}
\contentsline {section}{\numberline {6.1}Iteraci\IeC {\'o}n 1: Primeros pasos con Kinect}{84}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Primer programa con Kinect}{85}{subsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.1.1}La reflexi\IeC {\'o}n}{86}{subsubsection.6.1.1.1}
\contentsline {subsubsection}{\numberline {6.1.1.2}La oclusi\IeC {\'o}n}{87}{subsubsection.6.1.1.2}
\contentsline {subsubsection}{\numberline {6.1.1.3}Desalineamiento entre c\IeC {\'a}maras}{88}{subsubsection.6.1.1.3}
\contentsline {subsection}{\numberline {6.1.2}Apreciaciones a nivel de p\IeC {\'\i }xel}{88}{subsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.2.1}P\IeC {\'\i }xeles RGB}{89}{subsubsection.6.1.2.1}
\contentsline {subsubsection}{\numberline {6.1.2.2}P\IeC {\'\i }xeles de profundidad}{90}{subsubsection.6.1.2.2}
\contentsline {subsection}{\numberline {6.1.3}Conversi\IeC {\'o}n a distancias reales}{92}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Trabajando en tres dimensiones}{94}{subsection.6.1.4}
\contentsline {subsubsection}{\numberline {6.1.4.1}Nube de puntos}{96}{subsubsection.6.1.4.1}
\contentsline {subsection}{\numberline {6.1.5}Afianzando los conceptos adquiridos}{101}{subsection.6.1.5}
\contentsline {subsubsection}{\numberline {6.1.5.1}Dibujando con Kinect}{101}{subsubsection.6.1.5.1}
\contentsline {subsubsection}{\numberline {6.1.5.2}\IeC {\'A}lbum de fotos}{102}{subsubsection.6.1.5.2}
\contentsline {subsubsection}{\numberline {6.1.5.3}Bater\IeC {\'\i }a virtual}{103}{subsubsection.6.1.5.3}
\contentsline {section}{\numberline {6.2}Iteraci\IeC {\'o}n 2: Comprendiendo el funcionamiento de \acs {OpenNI}}{105}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Aspectos b\IeC {\'a}sicos sobre la librer\IeC {\'\i }a}{106}{subsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.1.1}openni::OpenNI}{107}{subsubsection.6.2.1.1}
\contentsline {paragraph}{\numberline {6.2.1.1.1}Acceso al dispositivo \newline }{107}{paragraph.6.2.1.1.1}
\contentsline {paragraph}{\numberline {6.2.1.1.2}Acceso a los flujos de v\IeC {\'\i }deo \newline }{108}{paragraph.6.2.1.1.2}
\contentsline {paragraph}{\numberline {6.2.1.1.3}Acceso a los dispositivos a trav\IeC {\'e}s de eventos \newline }{108}{paragraph.6.2.1.1.3}
\contentsline {paragraph}{\numberline {6.2.1.1.4}Informaci\IeC {\'o}n de errores \newline }{108}{paragraph.6.2.1.1.4}
\contentsline {subsubsection}{\numberline {6.2.1.2}openni::Device}{109}{subsubsection.6.2.1.2}
\contentsline {subsubsection}{\numberline {6.2.1.3}openni::VideoStream}{110}{subsubsection.6.2.1.3}
\contentsline {subsubsection}{\numberline {6.2.1.4}openni::VideoFrameRef}{110}{subsubsection.6.2.1.4}
\contentsline {subsection}{\numberline {6.2.2}GLUT}{110}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Primera versi\IeC {\'o}n del m\IeC {\'o}dulo de captura de im\IeC {\'a}genes}{113}{subsection.6.2.3}
\contentsline {section}{\numberline {6.3}Iteraci\IeC {\'o}n 3: Incorporaci\IeC {\'o}n de NiTE}{114}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Toma de contacto con NiTE}{116}{subsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.1.1}Seguimiento de manos y detecci\IeC {\'o}n de gestos}{116}{subsubsection.6.3.1.1}
\contentsline {subsubsection}{\numberline {6.3.1.2}Seguimiento del cuerpo}{118}{subsubsection.6.3.1.2}
\contentsline {paragraph}{\numberline {6.3.1.2.1}Funcionamiento \newline }{118}{paragraph.6.3.1.2.1}
\contentsline {paragraph}{\numberline {6.3.1.2.2}An\IeC {\'a}lisis e interpretaci\IeC {\'o}n de la informaci\IeC {\'o}n proporcionada \newline }{118}{paragraph.6.3.1.2.2}
\contentsline {subsection}{\numberline {6.3.2}Evoluci\IeC {\'o}n del m\IeC {\'o}dulo de captura de im\IeC {\'a}genes}{120}{subsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.2.1}Segunda versi\IeC {\'o}n}{121}{subsubsection.6.3.2.1}
\contentsline {subsubsection}{\numberline {6.3.2.2}Tercera versi\IeC {\'o}n}{126}{subsubsection.6.3.2.2}
\contentsline {subsubsection}{\numberline {6.3.2.3}Cuarta versi\IeC {\'o}n}{129}{subsubsection.6.3.2.3}
\contentsline {section}{\numberline {6.4}Iteraci\IeC {\'o}n 4: M\IeC {\'o}dulo de reconocimiento de acciones}{133}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Modelo Bag of Words}{134}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Implementaci\IeC {\'o}n de la soluci\IeC {\'o}n propuesta}{138}{subsection.6.4.2}
\contentsline {subsubsection}{\numberline {6.4.2.1}Fase 1: Configuraci\IeC {\'o}n inicial}{143}{subsubsection.6.4.2.1}
\contentsline {subsubsection}{\numberline {6.4.2.2}Fase 2: Segmentaci\IeC {\'o}n de v\IeC {\'\i }deos}{144}{subsubsection.6.4.2.2}
\contentsline {subsubsection}{\numberline {6.4.2.3}Procesamiento}{147}{subsubsection.6.4.2.3}
\contentsline {paragraph}{\numberline {6.4.2.3.1}Fase 3: Extracci\IeC {\'o}n de caracter\IeC {\'\i }sticas \newline }{148}{paragraph.6.4.2.3.1}
\contentsline {paragraph}{\numberline {6.4.2.3.2}Fase 4: Clustering \newline }{152}{paragraph.6.4.2.3.2}
\contentsline {paragraph}{\numberline {6.4.2.3.3}Fase 5: Proceso de reconocimiento \newline }{154}{paragraph.6.4.2.3.3}
\contentsline {subparagraph}{Etapa de training \newline }{156}{lstnumber.6.8.7}
\contentsline {subparagraph}{Etapa de testing \newline }{158}{lstnumber.6.8.7}
\contentsline {section}{\numberline {6.5}Iteraci\IeC {\'o}n 5: Interfaz gr\IeC {\'a}fica}{159}{section.6.5}
\contentsline {subparagraph}{Captura de v\IeC {\'\i }deo \newline }{160}{section.6.5}
\contentsline {subparagraph}{Etiquetado de v\IeC {\'\i }deo \newline }{160}{figure.6.50}
\contentsline {subparagraph}{Clasificaci\IeC {\'o}n \newline }{161}{figure.6.51}
\contentsline {subparagraph}{Inspector de resultados \newline }{161}{figure.6.52}
\contentsline {chapter}{\numberline {7}Resultados}{167}{chapter.7}
\contentsline {section}{\numberline {7.1}Dataset de entrenamiento empleado}{167}{section.7.1}
\contentsline {section}{\numberline {7.2}Recogida de datos de prueba}{168}{section.7.2}
\contentsline {section}{\numberline {7.3}An\IeC {\'a}lisis de los resultados}{171}{section.7.3}
\contentsline {chapter}{\numberline {8}Conclusiones y trabajo futuro}{181}{chapter.8}
\contentsline {section}{\numberline {8.1}Objetivos alcanzados}{181}{section.8.1}
\contentsline {section}{\numberline {8.2}Trabajo futuro}{184}{section.8.2}
\contentsline {chapter}{\numberline {9}Conclusions and future work}{187}{chapter.9}
\contentsline {section}{\numberline {9.1}Achieved objectives}{187}{section.9.1}
\contentsline {section}{\numberline {9.2}Future work}{190}{section.9.2}
\contentsline {chapter}{\numberline {A}Listados de los ejemplos con Processing}{195}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Ejemplo 1}{195}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Ejemplo 2}{196}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Primer ejemplo de nube de puntos}{197}{section.Alph1.3}
\contentsline {section}{\numberline {A.4}Primer ejemplo de nube de puntos con movimiento}{198}{section.Alph1.4}
\contentsline {section}{\numberline {A.5}Primer ejemplo de nube de puntos a color}{199}{section.Alph1.5}
\contentsline {section}{\numberline {A.6}Dibujando con Kinect}{200}{section.Alph1.6}
\contentsline {section}{\numberline {A.7}\IeC {\'A}lbum de fotos}{202}{section.Alph1.7}
\contentsline {section}{\numberline {A.8}Bater\IeC {\'\i }a musical}{204}{section.Alph1.8}
\contentsline {chapter}{\numberline {B}M\IeC {\'o}dulo de captura de im\IeC {\'a}genes}{209}{appendix.Alph2}
\contentsline {section}{\numberline {B.1}Dise\IeC {\~n}o definitivo}{209}{section.Alph2.1}
\contentsline {subsection}{\numberline {B.1.1}Modelo de clases definitivo}{209}{subsection.Alph2.1.1}
\contentsline {subsection}{\numberline {B.1.2}Estructura de directorio definitivo}{210}{subsection.Alph2.1.2}
\contentsline {subsection}{\numberline {B.1.3}Modelo de flujo de la ejecuci\IeC {\'o}n}{211}{subsection.Alph2.1.3}
\contentsline {chapter}{\numberline {C}Formulario de consentimiento}{213}{appendix.Alph3}
\contentsline {chapter}{\numberline {D}Conclusi\IeC {\'o}n personal}{219}{appendix.Alph4}
\contentsline {chapter}{Referencias}{221}{appendix*.47}
