\chapter{Introducción}

La búsqueda de espacios cada vez más inteligentes en los que el propio
entorno  dé soporte  a personas  de la  tercera edad  (Ambient Assited
Living),  personas  en  su  vida cotidiana  (Ambient  Intelligence)  o
ciudadanos (Smart  Cities) plantea, como  primer reto, el  desarrollo de
tecnologías o sistemas  capaces de identificar las  acciones que estas
personas están  llevando a cabo  en ese entorno. 

El  reconocimiento de  acciones humanas  es, sin  embargo, uno  de los
principales retos de la comunidad científica y muy especialmente en el
área de la visión por computador. En este sentido, la aproximación más
común  consiste   en  extraer  imágenes  características   de  vídeos y
caracterizar y etiquetar estas  imágenes con la correspondiente acción
que se  está realizando  en ellas.  Para ello se  suele recurrir  a un
algoritmo de  clasificación previamente  entrenado con las  acciones a
reconocer. 

Sin embargo,  el enfoque basado  en vídeo no es  el único. Así,  en el
estado  del arte  podemos  identificar diferentes  propuestas que  van
desde las más  intrusivas, como las basadas en  sensores desplegados a
lo largo del cuerpo ~\cite{Xu:2012:CHA:2223949.2224075} hasta otras como las basadas en el análisis
de  los   valores  obtenidos  por   los  sensores  de   los  teléfonos
móviles ~\cite{Anguita:2012:HAR:2437855.2437888}.   Pero, independientemente  del  enfoque aplicado,  la
realidad es  que el  reconocimiento de acciones  humanas es  una tarea
tremendamente compleja.

Esa complejidad, en el caso  particular del reconocimiento de acciones
humanas basadas en  el análisis de vídeo, se deriva  de los diferentes
retos que  esta tarea  debe abordar.   En primer  lugar hay  que hacer
frente a la  importante variabilidad en el rendimiento  obtenido en el
reconocimiento de distintas  acciones.  Un ejemplo de  ello podría ser
como en los  movimientos que implica el caminar, las  zancadas de cada
persona  pueden diferir  en  gran  medida.  Esto  se  debe  a que  los
movimientos  corporales implicados  en una  determinada acción  no son
únicos y no están perfectamente delimitados, variando de unas personas
a otras  e incluso dentro  de la misma  persona.  Esto supone  un gran
desafío  ya  que una  buena  aproximación  para el  reconocimiento  de
acciones debería ser capaz de generalizar sobre las variaciones dentro
de una clase y distinguir entre las acciones de diferentes clases.  De
este modo, cuando exista un gran  número de las clases de acción, esto
será aún más  complicado ya que el solapamiento entre  las clases será
mayor.

Otro reto que debe ser tenido en cuenta es la naturaleza cambiante del
entorno  donde   se  producen   las  acciones.  Esto   implica  serias
dificultades     en    diferentes     aspectos    que     deben    ser
considerados. Ejemplos de ellos podrían ser cómo partes de una persona
pueden  ser ocluidas  en las  grabaciones o  la influencia  que pueden
tener  las  condiciones  de  luminosidad   en  la  apariencia  de  una
persona. No obstante, esta tarea también  se encuentra con el reto que
suponen las  variaciones temporales. Por consiguiente,  la velocidad a
la  que  se registra  la  acción  tiene  un  efecto importante  en  la
extensión  temporal de  una acción,  especialmente cuando  se utilizan
funciones  de movimiento.  Un algoritmo  de reconocimiento  robusto de
acciones  humanas  debería  ser  invariante  a  diferentes  ritmos  de
ejecución.

Recientemente, algunos investigadores han utilizado cámaras RGBD, como
Microsoft  Kinect,   para  intentar  abordar  los   retos  mencionados
arriba. Estas  cámaras de profundidad  añaden una dimensión  extra que
las cámaras 2D normales no  proporcionan. De este modo, la información
sensorial proporcionada por este tipo  de cámaras se han empleado para
generar  modelos del  esqueleto  humano en  tiempo  real que  permiten
considerar las diferentes posiciones del cuerpo.  Estos modelos del
esqueleto proporciona información significativa que los investigadores
han usado para modelar las actividades  humanas que han sido usadas en
el entrenamiento  del algoritmo de clasificación  y que posteriormente
serán empleadas para el reconocimiento de actividades desconocidas.

Este trabajo está motivado por la  necesidad de abordar el problema de
cómo las posturas corporales inciden  en la tarea de reconocimiento de
acciones humanas  basadas en  el análisis  de vídeo.   De este  modo se
parte  de la  hipótesis de  trabajo de  que el  uso de  cámaras de
profundidad  puede ayudar  a mejorar  las tasas  de reconocimiento  de
acciones humanas  en las  que los movimientos  corporales no  han sido
predefinidos, sino que dependerán de la persona que los realice.  Este
trabajo      está      inspirado       en      el      trabajo      de
~\cite{Nebel:2011:CMC:2045195.2045230}  en  el   que  se  presenta  un
análisis  entre   diferentes  técnicas  para  abordar   la  tarea  del
reconocimiento de acciones,  demostrando que la técnica  que ofrece el
mayor equilibrio tiempo/rendimiento es la técnica de \acf{BoW} con una
precisión del 63.9\%. 

El modelo \acs{BoW}  surgió como una aproximación  motivada en métodos
de   categorización   de  texto   ~\cite{Joachims98textcategorization}
~\cite{Tong:2002:SVM:944790.944793}
~\cite{Lodhi:2002:TCU:944790.944799}
~\cite{Cristianini:2002:LSK:607586.607620}.    No  obstante,   se  han
publicado  estudios ~\cite{Zhu:2002:TKI:506309.506313}  en los  que la
idea  de adaptar  la categorización  de texto  a la  categorización de
vídeo puede resultar  verdaderamente interesante.  Es por  ello por lo
que   en   los  últimos   años   se   ha  demostrado   que   \acs{BoW}
~\cite{kaaniche:inria-00486110}      ~\cite{10.1109/CVPR.2008.4587527}
~\cite{LaptevP07}  es  uno  de  los   métodos  más  precisos  para  el
reconocimiento de  acciones, capaz de  actuar en una gran  variedad de
escenarios con un coste computacional  realmente bajo. Al contrario de
lo que sucede con otras  técnicas de clasificación de acciones humanas
~\cite{weinland:inria-00544741}     ~\cite{10.1109/CVPR.2008.4587737},
\acs{BoW} no  requiere ningún algoritmo adicional  de segmentación, lo
que  simplifica la  tarea de  visión por  computador haciendo,  por lo
tanto, posible trabajar directamente con datos de vídeo.

Utilizando la  metodología propuesta en  ~\cite{MartinezDelRincon:2013:CRH:2537174.2537391}, en
la que también se abordaba  el problema del reconocimiento de acciones
humanas realizadas de manera multimodal, este proyecto trabajará con un
sistema  entrenado con  un conjunto  de datos  (dataset) distinto  del
utilizado en la clasificación, comprobando  de esta manera la robustez
del   sistema   ante   la   multimodalidad  en   la   realización   de
acciones. Aunque el citado trabajo  presentaba un sistema combinado de
BoW y un  algoritmo de razonamiento, en él  se proporcionan resultados
del  rendimiento  obtenido por  el  algoritmo  BoW, entrenado  con  un
conjunto de  datos diferente al  utilizado en la  clasificación.  Este
trabajo utilizará esos datos para  compararlos con los obtenidos en un
experimento  similar, utilizando  cámaras de  profundidad en  lugar de
cámaras 2D.

Dado que  no hay vídeos estándar  adecuados para la descripción  de la
complejidad  de las  acciones  de  la vida  real  con  un conjunto  de
actividades complejas que sean  lo suficientemente representativas, se
procederá     a    la     creación     de     un    nuevo     dataset,
\textit{``KinbehrDataset''}.     \textit{``KinbehrDataset''}    estará
compuesto  por  acciones  realizadas  por diversos  individuos  en  un
entorno no controlado.  Para la  creación de dicho dataset se empleará
un método  científico de recogida de  datos que asegure la  validez de
las  muestras recogidas.   Además,  es importante  mencionar que  será
puesto a disposición  del resto de la comunidad científica  con el fin
de  que lo  resultados obtenidos  en  el presente  estudio puedan  ser
comparados con futuros  proyectos. De este modo,  se emplearán dataset
distintos para  el entrenamiento  y evaluación del  sistema propuesto.
Esto permitirá mostrar la generalidad  de la solución, sus capacidades
en  las diversas  aplicaciones de  la vida  real y  su rendimiento  en
situaciones complejas.

Por tratarse éste  de un proyecto con alto  contenido de investigación, la
elaboración  de  este trabajo  seguirá  la  siguiente metodología.  En
primer lugar  se realizará  un análisis de  las diferentes  cámaras de
profundidad que se  encuentran disponibles en el  mercado, teniendo en
cuenta   diferentes   aspectos   como   el   precio,   disponibilidad,
prestaciones,  soporte,  etc. Es  importante  destacar  que debido  a
limitaciones  presupuestarias  y  al  bajo  precio  que  ofrece,  este
análisis  estará limitado  a  los diferentes  modelos del  dispositivo
Kinect  de  Microsoft.  Seguidamente  se trataran  los  conceptos  más
relevantes  a  tener en  cuenta  en  el tratamiento,  procesamiento  e
interpretación  de  las  diferentes  imágenes  proporcionadas  por  el
dispositivo elegido. El siguiente paso a abordar será el desarrollo de
un módulo  que permita capturar  los diferentes flujos  de información
proporcionados  por  los sensores  del  dispositivo.  Para ello,  será
necesario seguir una  serie de pasos bien definidos,  entre los cuales
destacan la  realización de un  estudio de los  frameworks disponibles
para el dispositivo elegido, analizar las peculiaridades que presenta,
etc.

Seguidamente se  procederá a adaptar  la solución propuesta  al modelo
\acs{BoW}.  Para ello,  se dividirá  este proceso  en dos  etapas bien
diferenciadas. La  primera consistirá en el  entrenamiento del sistema
con un dataset  público que se adapte a los  objetivos perseguidos. De
este  modo,  se realizará  un  estudio  sobre los  diferentes  dataset
disponibles. Posteriormente,  se llevará a  cabo la segunda  etapa que
consistirá  en   la  evaluación   del  sistema  mediante   el  dataset
\textit{``KinbehrDataset''}, constituido  a partir de  grabaciones con
diferentes flujos  de información (imagen a  color, profundidad, etc.)
de  cada  individuo.  Finalmente,  se  procederá  a  la  validación  e
interpretación de  los resultados  obtenidos pudiendo comprobar  si la
hipótesis de trabajo con la que se comenzó se cumple.


\section{Estructura del documento}

El presente documento se estructura en siete capítulos bien diferenciados. El primero de ellos, donde el lector 
se encuentra en este momento, se ha mostrado una visión general sobre la hipótesis de trabajo desde la que se 
parte y así como la forma en la que se pretende abordar. En el capítulo de \textit{``Objetivos''} se presenta el 
objetivo general y los distintos objetivos específicos que se persiguen con la realización del \acs{TFG}.

En el capítulo de \textit{``Antecedentes''} se pretende ofrecer al lector una visión detallada sobre el dispositivo 
Kinect y el estado del arte de la visión por computador. El siguiente capítulo, \textit{``Método de trabajo''}, 
versa sobre la metodología de desarrollo empleada, la planificación del proyecto y las diferentes herramientas 
empleadas para su realización. Seguidamente, en el capítulo \textit{``Desarrollo del proyecto''}, que constituye el 
núcleo del documento, se expone el trabajo realizado en las diferentes iteraciones en las que se ha dividido el proyecto.

En los dos últimos capítulos, \textit{``Resultados''} y \textit{``Conclusiones y trabajo futuro''}, finalmente se 
exponen los resultados obtenidos en base a la hipótesis de trabajo de la que se partió y se analiza en que grado 
han sido logrados los objetivos, respectivamente.


% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
