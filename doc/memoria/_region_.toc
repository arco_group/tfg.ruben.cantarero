\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {v}}{chapter*.1}
\contentsline {chapter}{Abstract}{\es@scroman {vii}}{chapter*.3}
\contentsline {chapter}{Agradecimientos}{\es@scroman {ix}}{chapter*.4}
\contentsline {chapter}{\'Indice general}{\es@scroman {xiii}}{chapter*.5}
\contentsline {chapter}{\'{I}ndice de cuadros}{\es@scroman {xv}}{chapter*.6}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {xvii}}{chapter*.7}
\contentsline {chapter}{\IeC {\'I}ndice de listados}{\es@scroman {xix}}{chapter*.8}
\contentsline {chapter}{Listado de acr\IeC {\'o}nimos}{\es@scroman {xxi}}{chapter*.9}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Estructura del documento}{4}{section.1.1}
\contentsline {chapter}{\numberline {2}Objetivos}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Objetivo general}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Objetivos espec\IeC {\'\i }ficos}{5}{subsection.2.1.1}
\contentsline {chapter}{\numberline {3}Objectives}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}General objective}{9}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Specific objectives}{9}{subsection.3.1.1}
\contentsline {chapter}{\numberline {4}Antecedentes}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Kinect para Xbox 360}{11}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Introducci\IeC {\'o}n}{11}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}La creaci\IeC {\'o}n de Kinect}{12}{subsection.4.1.2}
\contentsline {subsubsection}{Precedentes}{13}{subsection.4.1.2}
\contentsline {subsubsection}{Evoluci\IeC {\'o}n}{15}{figure.4.4}
\contentsline {subsubsection}{Homebrew de Kinect}{19}{figure.4.7}
\contentsline {subsection}{\numberline {4.1.3}Desarrollo de aplicaciones alternativas}{22}{subsection.4.1.3}
\contentsline {subsubsection}{Ocio}{24}{figure.4.11}
\contentsline {subsubsection}{Medicina}{25}{figure.4.13}
\contentsline {subsubsection}{Educaci\IeC {\'o}n}{25}{figure.4.15}
\contentsline {subsubsection}{Accesibilidad}{26}{figure.4.18}
\contentsline {subsection}{\numberline {4.1.4}Caracter\IeC {\'\i }sticas t\IeC {\'e}cnicas}{28}{subsection.4.1.4}
\contentsline {subsubsection}{Segunda generaci\IeC {\'o}n de Kinect}{30}{figure.4.22}
\contentsline {subsection}{\numberline {4.1.5}Ventajas de Kinect frente a otros dispositivos}{32}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Funcionamiento}{32}{subsection.4.1.6}
\contentsline {subsubsection}{C\IeC {\'a}mara RGB}{32}{subsection.4.1.6}
\contentsline {subsubsection}{C\IeC {\'a}mara profundidad}{33}{figure.4.25}
\contentsline {subsubsection}{Sistema de micr\IeC {\'o}fonos}{35}{equation.4.1.1}
\contentsline {subsubsection}{Motor de inclinaci\IeC {\'o}n}{35}{figure.4.29}
\contentsline {subsection}{\numberline {4.1.7}\IeC {\textquestiondown }Por qu\IeC {\'e} Kinect?}{37}{subsection.4.1.7}
\contentsline {subsection}{\numberline {4.1.8}Librer\IeC {\'\i }as Kinect}{37}{subsection.4.1.8}
\contentsline {subsubsection}{SDK Windows}{38}{subsection.4.1.8}
\contentsline {paragraph}{Descripci\IeC {\'o}n del \acs {SDK}\newline }{38}{subsection.4.1.8}
\contentsline {subparagraph}{Plataformas soportadas\newline }{43}{figure.4.35}
\contentsline {paragraph}{Evoluci\IeC {\'o}n del framework\newline }{43}{figure.4.35}
\contentsline {paragraph}{Ventajas y desventajas\newline }{44}{figure.4.36}
\contentsline {subparagraph}{Ventajas\newline }{44}{figure.4.36}
\contentsline {subparagraph}{Desventajas\newline }{45}{figure.4.36}
\contentsline {subsubsection}{OpenNI}{45}{figure.4.36}
\contentsline {paragraph}{Descripci\IeC {\'o}n del framework\newline }{46}{figure.4.36}
\contentsline {subparagraph}{Plataformas soportadas\newline }{48}{figure.4.39}
\contentsline {subparagraph}{NiTE y otros middlewares de inter\IeC {\'e}s\newline }{49}{figure.4.39}
\contentsline {paragraph}{Evoluci\IeC {\'o}n del framework\newline }{51}{figure.4.42}
\contentsline {paragraph}{Ventajas y desventajas\newline }{53}{figure.4.43}
\contentsline {subparagraph}{Ventajas\newline }{53}{figure.4.43}
\contentsline {subparagraph}{Desventajas\newline }{53}{figure.4.43}
\contentsline {subsubsection}{OpenKinect}{54}{figure.4.43}
\contentsline {paragraph}{Descripci\IeC {\'o}n del framework\newline }{54}{figure.4.43}
\contentsline {paragraph}{Ventajas y desventajas\newline }{55}{figure.4.43}
\contentsline {subparagraph}{Ventajas\newline }{55}{figure.4.43}
\contentsline {subparagraph}{Desventajas\newline }{55}{figure.4.43}
\contentsline {section}{\numberline {4.2}Visi\IeC {\'o}n por computador}{56}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Perspectiva de visi\IeC {\'o}n por computador}{57}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Algoritmos de reconocimiento de acciones basados en v\IeC {\'\i }deo}{58}{subsection.4.2.2}
\contentsline {chapter}{\numberline {A}Listados de los ejemplos con Processing}{63}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Ejemplo 1}{63}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Ejemplo 2}{64}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Primer ejemplo de nube de puntos}{65}{section.Alph1.3}
\contentsline {section}{\numberline {A.4}Primer ejemplo de nube de puntos con movimiento}{66}{section.Alph1.4}
\contentsline {section}{\numberline {A.5}Primer ejemplo de nube de puntos a color}{67}{section.Alph1.5}
\contentsline {section}{\numberline {A.6}Dibujando con Kinect}{68}{section.Alph1.6}
\contentsline {section}{\numberline {A.7}\IeC {\'A}lbum de fotos}{70}{section.Alph1.7}
\contentsline {section}{\numberline {A.8}Bater\IeC {\'\i }a musical}{72}{section.Alph1.8}
