\contentsline {lstlisting}{\numberline {6.1}\IeC {\guillemotleft }Ejemplo 1\IeC {\guillemotright } versi\IeC {\'o}n 1}{86}{lstlisting.6.1}
\contentsline {lstlisting}{\numberline {6.2}\IeC {\guillemotleft }Dibujando con Kinect\IeC {\guillemotright } }{101}{lstlisting.6.2}
\contentsline {lstlisting}{\numberline {6.3}Detecci\IeC {\'o}n de poses con NiTE}{117}{lstlisting.6.3}
\contentsline {lstlisting}{\numberline {6.4}Ejemplo del formato \acs {PPM}.}{126}{lstlisting.6.4}
\contentsline {lstlisting}{\numberline {6.5}Estructura fichero \acs {XML}.}{134}{lstlisting.6.5}
\contentsline {lstlisting}{\numberline {6.6}Estructura fichero \textit {ACTOR\_frame\_segmented.txt}.}{145}{lstlisting.6.6}
\contentsline {lstlisting}{\numberline {6.7}Estructura fichero de datos del clasificador \acs {SVM}.}{156}{lstlisting.6.7}
\contentsline {lstlisting}{\numberline {6.8}Ejemplo de la estructura fichero de datos del clasificador \acs {SVM}.}{157}{lstlisting.6.8}
\contentsline {lstlisting}{\numberline {A.1}\IeC {\guillemotleft }Ejemplo 1\IeC {\guillemotright } ~\cite {borenstein2012making}}{195}{lstlisting.Alph1.1}
\contentsline {lstlisting}{\numberline {A.2}\IeC {\guillemotleft }Ejemplo 2\IeC {\guillemotright } ~\cite {borenstein2012making}}{196}{lstlisting.Alph1.2}
\contentsline {lstlisting}{\numberline {A.3}\IeC {\guillemotleft }Primer ejemplo Nube de puntos\IeC {\guillemotright } ~\cite {borenstein2012making}}{197}{lstlisting.Alph1.3}
\contentsline {lstlisting}{\numberline {A.4}\IeC {\guillemotleft }Primer ejemplo Nube de puntos con movimiento\IeC {\guillemotright } ~\cite {borenstein2012making}}{198}{lstlisting.Alph1.4}
\contentsline {lstlisting}{\numberline {A.5}\IeC {\guillemotleft }Primer ejemplo Nube de puntos a color\IeC {\guillemotright } ~\cite {borenstein2012making}}{199}{lstlisting.Alph1.5}
\contentsline {lstlisting}{\numberline {A.6}\IeC {\guillemotleft }Dibujando con Kinect\IeC {\guillemotright } ~\cite {borenstein2012making}}{200}{lstlisting.Alph1.6}
\contentsline {lstlisting}{\numberline {A.7}\IeC {\guillemotleft }\IeC {\'A}lbum de fotos\IeC {\guillemotright } ~\cite {borenstein2012making}}{202}{lstlisting.Alph1.7}
\contentsline {lstlisting}{\numberline {A.8}Clase Hotpoint de la aplicaci\IeC {\'o}n \IeC {\guillemotleft }Bater\IeC {\'\i }a musical\IeC {\guillemotright } ~\cite {borenstein2012making}}{204}{lstlisting.Alph1.8}
\contentsline {lstlisting}{\numberline {A.9}Clase principal de la aplicaci\IeC {\'o}n \IeC {\guillemotleft }Bater\IeC {\'\i }a musical\IeC {\guillemotright } ~\cite {borenstein2012making}}{205}{lstlisting.Alph1.9}
