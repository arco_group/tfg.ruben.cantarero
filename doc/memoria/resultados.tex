\chapter{Resultados}
\label{chap:resultados}

\drop{E}{n} el presente capítulo se describen los resultados obtenidos tras realizar las pruebas de precisión al sistema elaborado, además del proceso seguido para su correcta realización. Para ello se han empleado los 
módulos implementados a lo largo del proyecto (módulo de captura de imágenes y módulo de reconocimiento de acciones). De este modo, se ha decidido dividir el capítulo en las siguientes secciones:

\begin{itemize}
 \item Dataset de entrenamiento empleado.
 \item Recogida de datos de prueba.
 \item Análisis de los resultados.
\end{itemize}

Además, también se proporciona al lector la opción de acceder al repositorio (véase \url{https://bitbucket.org/arco_group/tfg.ruben.cantarero}) donde se encuentran todos los recursos generados a lo largo del proyecto.

\section{Dataset de entrenamiento empleado}

En primer lugar es necesario destacar que los vídeos empleados en el estudio deben representar escenarios lo más realistas posible. Esto se debe a que el principal objetivo es llevar a cabo un experimento de reconocimiento de
acciones que son relevantes en la vida real. Es por este motivo por lo que es muy importante elegir un conjunto de entrenamiento adecuado. De este modo el conjunto de entrenamiento debe cubrir la mayor 
variedad de puntos de vista de la cámara, proporcionando de este modo un entrenamiento independiente del punto de vista. Además, este conjunto también debe cubrir una cantidad suficientemente grande de casos que 
representen las acciones que interesan para el presente estudio. Por último este conjunto de datos debe estar etiquetado, segmentado y organizado.

Tras realizar una profunda búsqueda y análisis de los datasets disponibles se ha llegado a la conclusión de que los más adecuados y que mejor se ajustan a los requisitos buscados son \acf{IXMAS} ~\cite{Weinland:2006:FVA:1225844.1225855} 
y Hollywood ~\cite{10.1109/CVPR.2008.4587756}. El conjunto de entrenamiento Hollywood está orientado hacia la detección de eventos incluyendo acciones significativas pero independientes unas de otras (comer, besar, conducir un 
coche, correr, etc.). Por el contrario, el dataset \acs{IXMAS} está centrado en acciones realizadas en un entorno cerrado ofreciendo de este modo una descripción de las acciones posibles muy exhaustiva. Por lo tanto, las acciones 
ofrecidas en \acs{IXMAS} pueden ser combinadas para descibir actividades simples como dar patadas, puñetazos, andar, sentarse, etc. y además proporciona representaciones completas de un conjunto de acciones realizadas de forma 
individual. Asimismo, por ser un dataset ampliamente utilizado, permite comparar y validar el sistema propuesto.

Por consiguiente, se ha decidido emplear el dataset \acs{IXMAS} para realizar la etapa de entrenamiento del sistema. Este conjunto de entrenamiento está compuesto por trece tipos de acciones realizadas por doce actores diferentes. Además, 
cada acción se encuentra grabada de forma simultanea por cinco cámaras diferentes (ver Figura~\ref{fig:ixmas}). A continuación se muestra un listado de las acciones reconocidas:

\begin{itemize}
\label{acciones}
 \item \textbf{\textit{(0)} Nada}: No se está realizando ninguna acción. El usuario está parado.
 \item \textbf{\textit{(1)} Mirar reloj}: El usuario realiza la acción de mirar el reloj de su muñeca.
 \item \textbf{\textit{(2)} Cruzarse de brazos}: El usuario realiza la acción de cruzarse de brazos.
 \item \textbf{\textit{(3)} Rascarse la cabeza}: El usuario realiza la acción de rascarse la cabeza.
 \item \textbf{\textit{(4)} Sentarse}: El usuario realiza la acción de sentarse en el suelo.
 \item \textbf{\textit{(5)} Levantarse}: El usuario realiza la acción de levantarse.
 \item \textbf{\textit{(6)} Darse la vuelta}: El usuario realiza la acción de darse la vuelta sobre sí mismo.
 \item \textbf{\textit{(7)} Caminar}: El usuario realiza la acción de caminar.
 \item \textbf{\textit{(8)} Saludar}: El usuario realiza la acción de saludar con su mano.
 \item \textbf{\textit{(9)} Dar puñetazo}: El usuario realiza la acción de dar puñetazos.
 \item \textbf{\textit{(10)} Dar patada}: El usuario realiza la acción de dar patadas.
 \item \textbf{\textit{(11)} Señalar}: El usuario realiza la acción de señalar con el dedo.
 \item \textbf{\textit{(12)} Coger}: El usuario realiza la acción de coger algo del suelo, una mesa, etc.
 \item \textbf{\textit{(13)} Lanzar}: El usuario realiza la acción de lanzar algo sobre su cabeza.
 \item \textbf{\textit{(14)} Lanzar}: El usuario realiza la acción de lanzar algo de abajo hacia arriba.
\end{itemize}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.6\textwidth]{ixmas.jpg}
\caption{Ejemplo de \acs{IXMAS}. ~\cite{wbIxmas}}
\label{fig:ixmas}
\end{center}
\end{figure}


\section{Recogida de datos de prueba}

Como ya se anticipaba al comienzo del capítulo, se van a emplear dos datasets de acciones diferentes para evaluar el sistema: Dataset \acs{IXMAS} y uno capturado mediante el dispositivo Kinect. El primero de 
ellos será empleado para llevar a cabo la etapa de entrenamiento o \textit{``training''}, mientras que el segundo servirá como conjunto de datos para las pruebas o \textit{``testing''} (véase \S\,\ref{solucion_propuesta}). 
El principal motivo de esta decisión se basa en que mediante el uso de dos datasets considerablemente diferentes es posible mostrar la generalidad, las capacidades y el rendimiento del sistema desarrollado en el presente 
proyecto. 

Para recoger este segundo conjunto de datos para el proceso de \textit{testing} se ha empleado el módulo de captura de imágenes desarrollado en el proyecto (véase \S\,\ref{comprendiendo_el_funcionamiendo_de_openni} y 
\S\,\ref{incorporacion_de_nite}). De esta forma, ha sido posible generar un conjunto de entrenamiento propio: \textit{``KinbehrDataset''}. El primer paso necesario para la creación de este dataset fue la preparación del 
escenario en el que iban a tener lugar las grabaciones. Como se puede apreciar en la Figura~\ref{fig:escenario_vacio} el escenario consiste en una única habitación que dispone de los siguientes elementos:

\begin{itemize}
 \item \textbf{Un libro}: Consiste en un libro de crucigramas con la intención de captar la atención de los actores durante la grabación y potenciar así la realización de la acción coger. 
 \item \textbf{Una silla}: Puede ser utilizada y movida por los actores durante la grabación. De este modo se pretende animar a los actores a realizar la 
 acción sentarse.
 \item \textbf{Un saco de boxeo}: Puede ser empleada por los actores durante la grabación. La forma más natural de interaccionar con él es mediante puñetazos y patadas.
 \item \textbf{Una pelota}: Puede ser empleada por los actores durante la grabación. La forma más natural de interaccionar con ella es botándola o lanzarla al aire o contra las paredes para así realizar 
 la tarea de lanzar o tirar algo. 
\end{itemize}


\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.6\textwidth]{escenario_vacio.png}
\caption{Escenario de grabación empleado.}
\label{fig:escenario_vacio}
\end{center}
\end{figure}

Mediante la introducción de estos elementos en el escenario se pretende diseñar un dataset que sea lo más representativo posible respecto de la inmensa variabilidad de acciones de la vida real, sin perder en ningún momento de vista 
que debe contener la mayoría de las acciones integradas en el dataset \acs{IXMAS} con el que ha sido entrenado el sistema. De esta manera se conduce a los actores a que de forma natural interaccionen con los elementos que 
interesan para los objetivos del sistema. Un ejemplo de ello podría ser como el actor encuentra el libro, lo recoge (acción coger) y se sienta a leerlo (acción sentarse). Otro ejemplo también podría ser como el actor ve 
el saco de boxeo y empieza a jugar con el dándole patadas y puñetazos (acción patada y acción puñetazo).

Una vez que el escenario estaba preparado se comenzó con el proceso de grabación. Para ello se convocó a quince actores de diferentes estaturas, peso y sexo para obtener la mayor variedad posible. En el 
Cuadro~\ref{tab:informacion_actores} se muestra un resumen con la información más relevante.

\begin{table}[hp]
  \centering
  {\small
  \input{tables/informacion_actores.tex}
  }
  \caption[Información y número de acciones realizado por cada actor.]
  {Información y número de acciones realizado por cada actor.}
  \label{tab:informacion_actores}
\end{table}

Es importante mencionar que este proceso de grabación fue dividido en dos partes bien diferenciadas con objetivos diferentes. De esta forma, por cada actor se realizaban dos procesos de grabación:

\begin{itemize}
 \item \textbf{Grabación en entorno no guiado}: En este proceso de grabación no se le daba ningún tipo de instrucción al actor. De este modo, se dejaba al actor en la habitación con el lema \textit{``tienes 8 minutos para 
 hacer lo que quieras''}.
 \item \textbf{Grabación en entorno guiado}: En este proceso se iba solicitando al actor que realizara repetidamente las acciones que se le fueran indicando. Una de las principales motivaciones de este proceso es facilitar un 
 conjunto de datos que pueda ser posteriormente empleado para el entrenamiento del sistema.
\end{itemize}

Como resultado se obtuvieron dos paquetes de vídeos por cada actor capturados mediante el módulo de captura de imágenes. Estos paquetes están compuestos por tres tipos de vídeos cada uno: Vídeo RGB, vídeo de profundidad y vídeo 
de esqueleto. Posteriormente cada una de estas secuencias grabadas tuvieron que ser manualmente segmentadas y etiquetadas por acciones (véase \S\,\ref{segmentacion_de_videos}) mediante la herramienta elaborada en el presente 
proyecto (véase \S\,\ref{interfaz_gráfica}). Recuérdese que la segmentación automática de vídeos en acciones independientes queda fuera del alcance y objetivos de este proyecto. Los lectores interesados en la segmentación 
automática de vídeos puede referirse a ~\cite{conf/cvpr/RuiA00} ~\cite{conf/cvpr/BlackYJF97} ~\cite{conf/event/AliA01} ~\cite{conf/icra/ShimosakaMS07} y ~\cite{conf/cvpr/ShiWCS08}.

Por último es importante destacar que, como los vídeos de las personas pueden considerarse datos de carácter personal, fue necesario la elaboración de un formulario de consentimiento (véase \S\,\ref{chap:anexo4}). De esta 
forma, antes de comenzar con el proceso de grabación los actores debían leer un documento que les era facilitado. En este documento se explicaba el objetivo del proyecto, el uso de dicha grabación, etc. Además se les facilitaba 
respuesta a todas las preguntas que les pudiere surgir acerca del documento o del proceso. Finalmente se les proporcionaba un formulario que debían rellenar y firmar si estaban de acuerdo con el contrato. Para asegurar 
la confidencialidad, el único momento en el que quedan relacionados lo vídeos con los datos identificativos de los actores (nombres y apellidos) es en dicho formulario. De este modo, los vídeos fueron nombrados como actor1, actor2, 
etc. Finalmente estos formularios fueron almacenadas en el grupo ARCO, lugar de acceso restringido, para asegurar su seguridad.


\section{Análisis de los resultados}

Una vez que el dataset \textit{KinbehrDataset} ha sido generado, segmentado y etiquetado por acciones, es el momento de realizar el análisis de los resultados obtenidos. No obstante, antes es necesario aclarar algunos conceptos 
importantes que se mencionaron anteriormente. Estos son los valores de algunos de los parámetros empleados por las herramientas \textit{stipdept} (véase \S\,\ref{extraccion_de_caracteristicas}) y \textit{bsvm-train} 
(véase \S\,\ref{etapa_de_training}). Como \textit{a priori} no se conoce cuál es el valor más adecuado para el caso de estudio, se realizaron diferentes pruebas tomando diferentes valores. De este modo, se pudo determinar 
qué valores eran los más adecuados para estos parámetros y así poder obtener los mejores resultados posibles.

A continuación se expone el análisis dichos resultados. Estos muestran el porcentaje de acierto de cada actor según tipo de acción clasificados por la clase de vídeo (RGB, profundidad y esqueleto). En el
Cuadro~\ref{tab:porcentaje_acierto_por_actor} se muestra el análisis de resultados correspondientes a los vídeos obtenidos en el proceso de grabación no guiado y en el Cuadro~\ref{tab:porcentaje_acierto_por_actor_controlado} 
los correspondientes a los vídeos obtenidos en el proceso de grabación guiado. Es importante mencionar que los guiones representan que el actor correspondiente no ha realizado una determinada acción. Por último, también se 
muestra el porcentaje total de acierto del actor para todas las acciones que ha realizado.

\begin{table}[hp]
\scalebox{0.9}[0.9]{
  \centering
  {\small
  \input{tables/porcentaje_acierto_por_actor.tex}
  }
  }
  \caption[Porcentaje de acierto de cada actor (sin guiar) en cada tipo de acción según el tipo de imagen.]
  {Porcentaje de acierto de cada actor (sin guiar) en cada tipo de actor según el tipo de imagen.}
  \label{tab:porcentaje_acierto_por_actor}
\end{table}

\begin{table}[hp]
\scalebox{0.9}[0.9]{
  \centering
  {\small
  \input{tables/porcentaje_acierto_por_actor_controlado.tex}
  }
  }
  \caption[Porcentaje de acierto de cada actor (guiado) en cada tipo de acción según el tipo de imagen.]
  {Porcentaje de acierto de cada actor (guiado) en cada tipo de actor según el tipo de imagen.}
  \label{tab:porcentaje_acierto_por_actor_controlado}
\end{table}

En el Cuadro~\ref{tab:porcentaje_acierto_por_accion} se muestra un resumen del análisis de los resultados obtenidos según el tipo de acción. Estos resultados representan el porcentaje de acierto clasificados por la clase 
de vídeo (RGB, profundidad y esqueleto). En la Figura~\ref{fig:grafico_acciones} se muestra un gráfico que representa el porcentaje de acierto según la clase de vídeo para cada acción, considerando 
los vídeos grabados en el entorno guiado y no guiado.


\begin{figure}[!h]
\begin{center}
\includegraphics[width=1.1\textwidth]{grafico_acciones.png}
\caption{Porcentaje de acierto de cada acción según el tipo de imagen (entorno guiado y sin guiar).}
\label{fig:grafico_acciones}
\end{center}
\end{figure}


\begin{table}[hp]
  \centering
  {\small
  \input{tables/porcentaje_acierto_por_accion.tex}
  }
  \caption[Porcentaje de acierto de cada acción según el tipo de imagen (entorno guiado y sin guiar).]
  {Porcentaje de acierto de cada acción según el tipo de imagen (entorno guiado y sin guiar).}
  \label{tab:porcentaje_acierto_por_accion}
\end{table}

En el Cuadro~\ref{tab:matriz_confusion_rgb}, el Cuadro~\ref{tab:matriz_confusion_depth} y el Cuadro~\ref{tab:matriz_confusion_esqueleto} se muestran las matrices de confusión correspondientes a cada tipo de vídeo, RGB, profundidad 
y esqueleto respectivamente. En el campo de la inteligencia artificial una matriz de confusión es una herramienta de visualización que se emplea en aprendizaje supervisado. Cada columna de la matriz representa el número de 
predicciones de cada clase, mientras que cada fila representa a las instancias en la clase real. Uno de los beneficios de las matrices de confusión es que facilitan ver si el sistema está confundiendo dos clases.

No obstante, es importante tener en cuenta que si en este tipo de matrices el número de muestras de clases diferentes cambia mucho la tasa de error del clasificador no es representativa de lo bien que realiza la tarea el 
clasificador. Esto quiere decir que si por ejemplo hay 1000 muestras de la clase 1 y sólo 20 de la clase 2, el clasificador puede tener fácilmente un sesgo hacia la clase 1. Si el clasificador clasifica todas las muestras como clase 1 su 
precisión será del 99\%. Esto no significa que sea un buen clasificador, pues tuvo un 100\% de error en la clasificación de las muestras de la clase 2. En este caso en particular puede resultar problemático ya 
que la acción que contiene mas ocurrencias (acción dar puñetazo con un valor de 453) difiere bastante con respecto la menor (acción señalar con un valor de 21).

\begin{table}[htbp]
\begin{center}
\scalebox{0.75}[0.75]{
\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
 \hline
 \tabheadformat
 \tabhead{} & 
 \begin{sideways} \tabhead{Mirar Reloj} \end{sideways} & 
 \begin{sideways} \tabhead{Cruzar Brazos} \end{sideways}&
 \begin{sideways} \tabhead{Rascar cabeza} \end{sideways}& 
 \begin{sideways} \tabhead{Sentarse} \end{sideways}&
 \begin{sideways} \tabhead{Levantarse} \end{sideways}&
 \begin{sideways} \tabhead{Darse la vuelta} \end{sideways}& 
 \begin{sideways} \tabhead{Caminar} \end{sideways}& 
 \begin{sideways} \tabhead{Saludar} \end{sideways}&
 \begin{sideways} \tabhead{Dar puñetazo} \end{sideways}& 
 \begin{sideways} \tabhead{Dar patada} \end{sideways}& 
 \begin{sideways} \tabhead{Señalar} \end{sideways}& 
 \begin{sideways} \tabhead{Coger} \end{sideways}& 
 \begin{sideways} \tabhead{Lanzar (encima)} \end{sideways}& 
 \begin{sideways} \tabhead{Lanzar (debajo)} \end{sideways}\\ 
 \hline
\textit{Mirar reloj} & \cellcolor[rgb]{0,1,1} 0.12 & 0.12 & 0.00 & 0.12 & 0.12 & 0.04 & 0.00 & 0.08 & 0.16 & 0.04 & 0.08 & 0.00 & 0.12 & 0.00 \\ \hline
\textit{Cruzar brazos} & 0.00 & \cellcolor[rgb]{0,1,1}0.05 & 0.00 & 0.05 & 0.23 & 0.05 & 0.00 & 0.09 & 0.45 & 0.05 & 0.00 & 0.00 & 0.05 & 0.00 \\ \hline
\textit{Rascar cabeza} & 0.04 & 0.00 & \cellcolor[rgb]{0,1,1}0.00 & 0.04 & 0.00 & 0.04 & 0.00 & 0.13 & 0.35 & 0.09 & 0.09 & 0.00 & 0.22 & 0.00 \\ \hline
\textit{Sentarse} & 0.00 & 0.00 & 0.03 & \cellcolor[rgb]{0,1,1}0.28 & 0.00 & 0.03 & 0.00 & 0.07 & 0.30 & 0.07 & 0.13 & 0.03 & 0.07 & 0.00 \\ \hline
\textit{Levantarse} & 0.00 & 0.00 & 0.03 & 0.00 & \cellcolor[rgb]{0,1,1}0.61 & 0.00 & 0.00 & 0.05 & 0.13 & 0.02 & 0.00 & 0.11 & 0.05 & 0.00 \\ \hline
\textit{Darse la vuelta} & 0.00 & 0.00 & 0.00 & 0.02 & 0.03 & \cellcolor[rgb]{0,1,1}0.17 & 0.17 & 0.11 & 0.18 & 0.26 & 0.02 & 0.00 & 0.04 & 0.00 \\ \hline
\textit{Caminar} & 0.00 & 0.00 & 0.00 & 0.01 & 0.01 & 0.11 & \cellcolor[rgb]{0,1,1}0.54 & 0.10 & 0.06 & 0.14 & 0.03 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Saludar} & 0.00 & 0.00 & 0.07 & 0.07 & 0.00 & 0.04 & 0.11 & \cellcolor[rgb]{0,1,1}0.15 & 0.11 & 0.15 & 0.04 & 0.00 & 0.26 & 0.00 \\ \hline
\textit{Dar puñetazo} & 0.03 & 0.00 & 0.00 & 0.07 & 0.01 & 0.04 & 0.17 & 0.04 & \cellcolor[rgb]{0,1,1}0.38 & 0.18 & 0.01 & 0.00 & 0.06 & 0.00 \\ \hline
\textit{Dar patada} & 0.01 & 0.00 & 0.00 & 0.08 & 0.02 & 0.08 & 0.10 & 0.03 & 0.28 & \cellcolor[rgb]{0,1,1}0.33 & 0.05 & 0.00 & 0.02 & 0.00 \\ \hline
\textit{Señalar} & 0.00 & 0.10 & 0.00 & 0.10 & 0.10 & 0.05 & 0.00 & 0.14 & 0.24 & 0.14 & \cellcolor[rgb]{0,1,1}0.10 & 0.00 & 0.05 & 0.00 \\ \hline
\textit{Coger} & 0.00 & 0.00 & 0.01 & 0.03 & 0.07 & 0.04 & 0.05 & 0.03 & 0.20 & 0.05 & 0.07 & \cellcolor[rgb]{0,1,1}0.41 & 0.03 & 0.00 \\ \hline
\textit{Lanzar (encima)} & 0.01 & 0.00 & 0.00 & 0.02 & 0.02 & 0.12 & 0.11 & 0.06 & 0.34 & 0.23 & 0.03 & 0.03 & \cellcolor[rgb]{0,1,1}0.02 & 0.00 \\ \hline
\textit{Lanzar (debajo)} & 0.03 & 0.04 & 0.00 & 0.04 & 0.04 & 0.11 & 0.16 & 0.07 & 0.32 & 0.15 & 0.03 & 0.00 & 0.03 & \cellcolor[rgb]{0,1,1}0.00 \\ \hline
\end{tabular}
}
\end{center}
\caption{Matriz de confusión de los vídeos RGB (entorno guiado y sin guiar).}
\label{tab:matriz_confusion_rgb}
\end{table}


\begin{table}[htbp]
\begin{center}
\scalebox{0.75}[0.75]{
\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
 \tabheadformat
 \tabhead{} & 
 \begin{sideways} \tabhead{Mirar Reloj} \end{sideways} & 
 \begin{sideways} \tabhead{Cruzar Brazos} \end{sideways}&
 \begin{sideways} \tabhead{Rascar cabeza} \end{sideways}& 
 \begin{sideways} \tabhead{Sentarse} \end{sideways}&
 \begin{sideways} \tabhead{Levantarse} \end{sideways}&
 \begin{sideways} \tabhead{Darse la vuelta} \end{sideways}& 
 \begin{sideways} \tabhead{Caminar} \end{sideways}& 
 \begin{sideways} \tabhead{Saludar} \end{sideways}&
 \begin{sideways} \tabhead{Dar puñetazo} \end{sideways}& 
 \begin{sideways} \tabhead{Dar patada} \end{sideways}& 
 \begin{sideways} \tabhead{Señalar} \end{sideways}& 
 \begin{sideways} \tabhead{Coger} \end{sideways}& 
 \begin{sideways} \tabhead{Lanzar (encima)} \end{sideways}& 
 \begin{sideways} \tabhead{Lanzar (debajo)} \end{sideways}\\ 
 \hline
\textit{Mirar reloj} & \cellcolor[rgb]{0,1,1}0.00 & 0.00 & 0.00 & 0.05 & 0.00 & 0.00 & 0.71 & 0.00 & 0.00 & 0.10 & 0.14 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Cruzar brazos} & 0.00 & \cellcolor[rgb]{0,1,1}0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.83 & 0.00 & 0.00 & 0.11 & 0.06 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Rascar cabeza} & 0.00 & 0.00 & \cellcolor[rgb]{0,1,1}0.00 & 0.00 & 0.00 & 0.00 & 0.74 & 0.00 & 0.00 & 0.11 & 0.11 & 0.00 & 0.05 & 0.00 \\ \hline
\textit{Sentarse} & 0.00 & 0.00 & 0.00 & \cellcolor[rgb]{0,1,1}0.07 & 0.00 & 0.02 & 0.31 & 0.02 & 0.02 & 0.34 & 0.09 & 0.09 & 0.05 & 0.00 \\ \hline
\textit{Levantarse} & 0.02 & 0.00 & 0.00 & 0.00 & \cellcolor[rgb]{0,1,1}0.27 & 0.05 & 0.16 & 0.02 & 0.04 & 0.05 & 0.02 & 0.38 & 0.00 & 0.00 \\ \hline
\textit{Darse la vuelta} & 0.01 & 0.00 & 0.00 & 0.02 & 0.02 & \cellcolor[rgb]{0,1,1}0.04 & 0.36 & 0.09 & 0.09 & 0.35 & 0.02 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Caminar} & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.04 & \cellcolor[rgb]{0,1,1}0.74 & 0.11 & 0.01 & 0.09 & 0.00 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Saludar} & 0.00 & 0.00 & 0.00 & 0.00 & 0.04 & 0.09 & 0.35 & \cellcolor[rgb]{0,1,1}0.00 & 0.04 & 0.35 & 0.13 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Dar puñetazo} & 0.00 & 0.00 & 0.00 & 0.09 & 0.03 & 0.04 & 0.16 & 0.05 & \cellcolor[rgb]{0,1,1}0.29 & 0.27 & 0.03 & 0.00 & 0.04 & 0.00 \\ \hline
\textit{Dar patada} & 0.01 & 0.00 & 0.00 & 0.06 & 0.03 & 0.04 & 0.11 & 0.02 & 0.14 & \cellcolor[rgb]{0,1,1}0.54 & 0.02 & 0.00 & 0.04 & 0.00 \\ \hline
\textit{Señalar} & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.00 & 0.79 & 0.00 & 0.00 & 0.21 & \cellcolor[rgb]{0,1,1}0.00 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Coger} & 0.00 & 0.00 & 0.00 & 0.02 & 0.05 & 0.07 & 0.09 & 0.00 & 0.06 & 0.26 & 0.06 & \cellcolor[rgb]{0,1,1}0.38 & 0.00 & 0.00 \\ \hline
\textit{Lanzar (encima)} & 0.00 & 0.00 & 0.00 & 0.03 & 0.02 & 0.02 & 0.25 & 0.06 & 0.18 & 0.31 & 0.09 & 0.02 & \cellcolor[rgb]{0,1,1}0.01 & 0.00 \\ \hline
\textit{Lanzar (debajo)} & 0.01 & 0.00 & 0.00 & 0.06 & 0.06 & 0.04 & 0.19 & 0.08 & 0.23 & 0.28 & 0.04 & 0.01 & 0.00 & \cellcolor[rgb]{0,1,1}0.00 \\ \hline
\end{tabular}
}
\end{center}
\caption{Matriz de confusión de los vídeos de profundidad (entorno guiado y sin guiar).}
\label{tab:matriz_confusion_depth}
\end{table}


\begin{table}[htbp]
\begin{center}
\scalebox{0.75}[0.75]{
\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
 \tabheadformat
 \tabhead{} & 
 \begin{sideways} \tabhead{Mirar Reloj} \end{sideways} & 
 \begin{sideways} \tabhead{Cruzar Brazos} \end{sideways}&
 \begin{sideways} \tabhead{Rascar cabeza} \end{sideways}& 
 \begin{sideways} \tabhead{Sentarse} \end{sideways}&
 \begin{sideways} \tabhead{Levantarse} \end{sideways}&
 \begin{sideways} \tabhead{Darse la vuelta} \end{sideways}& 
 \begin{sideways} \tabhead{Caminar} \end{sideways}& 
 \begin{sideways} \tabhead{Saludar} \end{sideways}&
 \begin{sideways} \tabhead{Dar puñetazo} \end{sideways}& 
 \begin{sideways} \tabhead{Dar patada} \end{sideways}& 
 \begin{sideways} \tabhead{Señalar} \end{sideways}& 
 \begin{sideways} \tabhead{Coger} \end{sideways}& 
 \begin{sideways} \tabhead{Lanzar (encima)} \end{sideways}& 
 \begin{sideways} \tabhead{Lanzar (debajo)} \end{sideways}\\ 
 \hline
\textit{Mirar reloj} & \cellcolor[rgb]{0,1,1}0.00 & 0.00 & 0.00 & 0.05 & 0.00 & 0.00 & 0.55 & 0.00 & 0.05 & 0.32 & 0.05 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Cruzar brazos} & 0.00 & \cellcolor[rgb]{0,1,1}0.00 & 0.00 & 0.00 & 0.00 & 0.06 & 0.67 & 0.06 & 0.00 & 0.22 & 0.00 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Rascar cabeza} & 0.00 & 0.00 & \cellcolor[rgb]{0,1,1}0.00 & 0.00 & 0.00 & 0.00 & 0.47 & 0.00 & 0.00 & 0.53 & 0.00 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Sentarse} & 0.00 & 0.00 & 0.00 & \cellcolor[rgb]{0,1,1}0.16 & 0.00 & 0.02 & 0.20 & 0.00 & 0.02 & 0.45 & 0.09 & 0.05 & 0.00 & 0.00 \\ \hline
\textit{Levantarse} & 0.00 & 0.00 & 0.00 & 0.00 & \cellcolor[rgb]{0,1,1}0.45 & 0.00 & 0.11 & 0.00 & 0.11 & 0.02 & 0.04 & 0.28 & 0.00 & 0.00 \\ \hline
\textit{Darse la vuelta} & 0.00 & 0.00 & 0.00 & 0.01 & 0.02 & \cellcolor[rgb]{0,1,1}0.05 & 0.23 & 0.10 & 0.12 & 0.38 & 0.04 & 0.00 & 0.02 & 0.00 \\ \hline
\textit{Caminar} & 0.00 & 0.00 & 0.00 & 0.00 & 0.01 & 0.01 & \cellcolor[rgb]{0,1,1}0.64 & 0.14 & 0.02 & 0.17 & 0.00 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Saludar} & 0.00 & 0.00 & 0.00 & 0.05 & 0.00 & 0.09 & 0.45 & \cellcolor[rgb]{0,1,1}0.05 & 0.09 & 0.18 & 0.05 & 0.00 & 0.05 & 0.00 \\ \hline
\textit{Dar puñetazo} & 0.01 & 0.00 & 0.00 & 0.05 & 0.04 & 0.04 & 0.23 & 0.07 & \cellcolor[rgb]{0,1,1}0.29 & 0.19 & 0.04 & 0.01 & 0.03 & 0.00 \\ \hline
\textit{Dar patada} & 0.01 & 0.00 & 0.00 & 0.04 & 0.05 & 0.03 & 0.17 & 0.02 & 0.09 & \cellcolor[rgb]{0,1,1}0.43 & 0.09 & 0.01 & 0.06 & 0.00 \\ \hline
\textit{Señalar} & 0.00 & 0.00 & 0.00 & 0.05 & 0.00 & 0.05 & 0.53 & 0.00 & 0.00 & 0.32 & \cellcolor[rgb]{0,1,1}0.05 & 0.00 & 0.00 & 0.00 \\ \hline
\textit{Coger} & 0.00 & 0.00 & 0.00 & 0.00 & 0.01 & 0.03 & 0.06 & 0.02 & 0.10 & 0.50 & 0.13 & \cellcolor[rgb]{0,1,1}0.12 & 0.02 & 0.00 \\ \hline
\textit{Lanzar (encima)} & 0.00 & 0.00 & 0.00 & 0.06 & 0.01 & 0.03 & 0.21 & 0.04 & 0.21 & 0.32 & 0.04 & 0.00 & \cellcolor[rgb]{0,1,1}0.07 & 0.00 \\ \hline
\textit{Lanzar (debajo)} & 0.00 & 0.00 & 0.00 & 0.12 & 0.05 & 0.09 & 0.21 & 0.03 & 0.23 & 0.21 & 0.03 & 0.00 & 0.04 & \cellcolor[rgb]{0,1,1}0.00 \\ \hline
\end{tabular}
}
\end{center}
\caption{Matriz de confusión de los vídeos de los esqueletos (entorno guiado y sin guiar).}
\label{tab:matriz_confusion_esqueleto}
\end{table}

\newpage

Llegados a este punto es el momento de contrastar y validar los resultados obtenidos y que se han explicado en los párrafos anteriores con sistemas similares cuyo objetivo final es también el reconocimiento de 
acciones. Para ello se ha tomado como referencia el estudio efectuado por Jesús Martínez Del Rincón, Jean-Christophe Nebel y María Jesús Santofimia Romero: \textit{``Common-sense reasoning for human 
action recognition''} ~\cite{MartinezDelRincon:2013:CRH:2537174.2537391}. En dicho estudio se presenta un nuevo método que aprovecha las capacidades de razonamiento en un sistema de
visión por computador dedicado al reconocimiento de la acción humana. Para ello, proponen una metodología dividida en dos etapas. La primera de ellas está basada en los mismos principios   
que el presente proyecto: \acf{BoW}. Una vez que han obtenido los resultados proporcionados en la etapa de aprendizaje basado en el algoritmo máquina, realizan un procesamiento adicional. Este 
procesamiento consiste en el análisis y corrección de las estimaciones iniciales proporcionadas por \acs{BoW} mediante un sistema de razonamiento común.

En dicho estudio generan su propio dataset (\textit{WaRo11}) para obtener los resultados mediante una cámara RGB normal. En el Cuadro~\ref{tab:comparacion_de_resultados} se muestra una tabla comparativa de 
los resultados obtenidos en dicho estudio con respecto a los obtenidos en el presente proyecto. A continuación se presenta una breve descripción de la tabla para facilitar su comprensión:

\begin{itemize}
 \item La primera columna representa que el sistema ~\cite{MartinezDelRincon:2013:CRH:2537174.2537391} ha sido entrenado con el dataset \acs{IXMAS} y evaluado con \textit{WaRo11}.
 \item La segunda columna representa que el sistema \acs{KinBehR} ha sido entrenado con el dataset \acs{IXMAS} y evaluado con \textit{KinbehrDataset}.
 \item La fila representa que se ha empleado únicamente la etapa de \acs{BoW}.
\end{itemize}


\begin{table}[htbp]
\begin{center}
\scalebox{0.7}[0.7]{
\begin{tabular}{p{.24\textwidth}p{.35\textwidth}p{.42\textwidth}}
  \tabheadformat
  \tabhead{}   &
  \tabhead{Tra. == \acs{IXMAS} \& Test. == WaRo11} &
  \tabhead{Tra. == \acs{IXMAS} \& Test. == KinbehrDataset}   \\
\hline
\textit{\acs{BoW}} &  RGB: 29.4 & RGB: 32.7 \\
\hline
\end{tabular}
}
\end{center}
\caption{Comparación de resultados con ~\cite{MartinezDelRincon:2013:CRH:2537174.2537391}.}
\label{tab:comparacion_de_resultados}
\end{table}


Un aspecto a destacar es que los resultados obtenidos con \acs{KinBehR} 
son algo mejores con respecto a los obtenidos en ~\cite{MartinezDelRincon:2013:CRH:2537174.2537391} en la etapa de \acs{BoW}. Es importante mencionar que los vídeos contenidos en el dataset 
\textit{KinbehrDataset} son bastante más complejos de evaluar que \textit{WaRo11}. Esto se debe principalmente a que los actores en \textit{KinbehrDataset} se comportan de una forma más natural, 
como por ejemplo realizar varias acciones al mismo tiempo. 

Por otro lado, en el Cuadro~\ref{tab:comparacion_de_resultados2} se puede observar que los resultados mejoran cuando el sistema es evaluado
con el mismo dataset con el que ha sido entrenado. No obstante, estos resultados no son del todo realistas ya que estas circunstancias no se 
van a dar en un entorno real en el que sea necesario realizar un reconocimiento de acciones. Otro aspecto a destacar es 
que los resultados obtenidos cuando se emplea un sistema de razonamiento adicional (segunda y tercera fila donde \textit{n} es el número de 
acciones considerado en el sistema de razonamiento) son considerablemente mejores. Por lo tanto, es muy posible 
que mediante la incorporación de un sistema de razonamiento en \acs{KinBehR} se obtenga una tasa de acierto mejorada. Este aspecto será comentado 
en más profundidad en el capítulo \textit{Conclusiones y trabajo futuro} (véase \S\,\ref{chap:conclusiones}).

\begin{table}[htbp]
\begin{center}
\scalebox{0.7}[0.7]{
\begin{tabular}{p{.24\textwidth}p{.35\textwidth}p{.35\textwidth}p{.42\textwidth}}
  \tabheadformat
  \tabhead{}   &
  \tabhead{Tra. \& Test. == \acs{IXMAS}}   &
  \tabhead{Tra. == \acs{IXMAS} \& Test. == WaRo11} &
  \tabhead{Tra. == \acs{IXMAS} \& Test. == KinbehrDataset}   \\
\hline
		     & RGB: 63.9 &  RGB: 29.4 & RGB: 32.7 \\
\textit{\acs{BoW}}   & Profundidad: - & Profundidad: - & Profundidad: 30.8 \\
		     & Esqueleto: - & Esqueleto: - & Esqueleto: 28 \\
\hline
\textit{\acs{BoW} + AIRS \textbf{(n = 1)}} & RGB: - & RGB: 35.5 & RGB: -\\
\hline
\textit{\acs{BoW} + AIRS \textbf{(n = 5)}} & RGB: - & RGB: 51.9 & RGB: -\\				 
\hline
\end{tabular}
}
\end{center}
\caption{Comparación de resultados con ~\cite{MartinezDelRincon:2013:CRH:2537174.2537391}.}
\label{tab:comparacion_de_resultados2}
\end{table}



Finalmente, para concluir el capítulo es importante explicar algunos de los motivos por los que la tasa de acierto no es mayor en algunos tipos de acciones. Los factores identificados más importantes son los siguientes:

\begin{itemize}
\item En los vídeos de profundidad capturados se ha comprobado que en ciertas ocasiones el middleware NiTE (véase \S\,\ref{toma_de_contacto_con_nite}) identifica  al saco de boxeo como una persona. Esto 
puede introducir ruido que dificulte la tarea de clasificación ya que como se aprecia en la Figura~\ref{fig:problema_depth} se introducen en la escena elementos que no son personas. Este problema se ve acentuado si el saco de 
boxeo se encuentra en movimiento.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.6\textwidth]{problema_depth.png}
\caption{Problema encontrado en vídeos de profundidad.}
\label{fig:problema_depth}
\end{center}
\end{figure}

\item Un problema muy similar al anterior ocurre con los vídeo de esqueleto. En la Figura~\ref{fig:problema_esqueleto} se aprecia como el middleware NiTE identifica el saco de boxeo como una persona y por lo tanto es 
dibujado su ``correspondiente'' esqueleto.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.6\textwidth]{problema_esqueleto.png}
\caption{Problema encontrado en vídeos de esqueletos.}
\label{fig:problema_esqueleto}
\end{center}
\end{figure}

\item El dataset \acs{IXMAS} etiqueta ciertas acciones de una manera muy particular permitiendo un margen muy pequeño en algunas acciones de las secuencias de vídeos capturadas. Uno de los ejemplos más llamativos es la ``acción 
sentarse''. En \acs{IXMAS} esta acción es siempre etiquetada con un actor sentándose en el suelo, mientras que en las pruebas realizadas se ha etiquetado como ``sentarse'' cuando un actor se sienta en la silla proporcionada 
en el escenario.

\end{itemize}




% Local Variables:
%  coding: utf-8
%  mode: latex
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
